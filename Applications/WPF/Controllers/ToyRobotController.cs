﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Models;
using ToyRobotChallenge.Library.AppSupport.UserCommands;

namespace WPFToyRobotChallenge.Controllers
{
    public class ToyRobotController
    {
        private readonly IMediator mediator;

        public ToyRobotController(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async Task<ToyRobotModel> ExecuteAsync(string command)
        {
            var commandLine = new CommandList(command, true);
            var robot = await this.mediator.Send(new Execute(commandLine));

            return robot;
        }

        internal async Task StartSimulationAsync(int tableWidth, int tableHeight)
        {
            await this.mediator.Send(new Start((ulong)tableWidth, (ulong)tableHeight));
        }
    }
}
