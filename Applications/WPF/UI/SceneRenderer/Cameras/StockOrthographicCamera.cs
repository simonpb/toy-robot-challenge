﻿using System.Windows.Media.Media3D;

namespace WPFToyRobotChallenge.UI.SceneRenderer.Cameras
{
    /// <summary>
    /// Stock orthographic camera
    /// </summary>
    public class StockOrthographicCamera
    {
        Camera camera;

        public static implicit operator Camera(StockOrthographicCamera rhs)
        {
            return rhs.camera;
        }

        public StockOrthographicCamera()
        {
            Create();
        }

        private void Create()
        {
            /* 
                <OrthographicCamera
                        LookDirection="0,0,-1" 
                        UpDirection="0,1,0" 
                        NearPlaneDistance="0" 
                        FarPlaneDistance="10"
                        Width="5"
                        Position="0,0,0"/>
             */

            this.camera = new OrthographicCamera()
            {
                Position = new Point3D(0.0, 0.0, 0.0),
                LookDirection = new Vector3D(0.0, 0.0, -1.0),
                UpDirection = new Vector3D(0.0, 1.0, 0.0),
                NearPlaneDistance = 0,
                FarPlaneDistance = 10,
                Width = 5
            };
        }
    }
}
