﻿using System.Windows.Media.Media3D;

namespace WPFToyRobotChallenge.UI.SceneRenderer
{
    /// <summary>
    /// All renderables managed by the <see cref="RenderEngine"/> must derive from this class
    /// </summary>
    public abstract class Renderable
    {
        protected ModelVisual3D model;

        /// <summary>
        /// implicit conversion from <see cref="Renderable"/> to a <see cref="ModelVisual3D"/>
        /// </summary>
        /// <param name="rhs"></param>
        public static implicit operator ModelVisual3D(Renderable rhs)
        {
            return rhs.model;
        }
    }
}
