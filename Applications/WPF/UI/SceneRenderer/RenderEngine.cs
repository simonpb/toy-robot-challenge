﻿using System.Collections.Generic;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Media;
using ToyRobotChallenge.Library.AppSupport.Models;
using WPFToyRobotChallenge.UI.SceneRenderer.Cameras;
using WPFToyRobotChallenge.UI.SceneRenderer.Lighting;
using WPFToyRobotChallenge.UI.SceneRenderer.Models;

namespace WPFToyRobotChallenge.UI.SceneRenderer
{
    public class RenderEngine
    {
        private IList<Renderable> renderables = new List<Renderable>();
        private Viewport3D viewPort;

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="viewPort"></param>
        public RenderEngine(Viewport3D viewPort)
        {
            this.viewPort = viewPort;

            InitialiseScene();
        }

        private void InitialiseScene()
        {
            // create (and add) the 1 and ONLY camera
            {
                var camera = new StockOrthographicCamera();
                this.viewPort.Camera = camera;
            }

            // create default scene elements
            {
                var table = new Table(5, 5, 5, 5);
                var lightSource = new AmbientLightSource(Color.FromRgb(255, 255, 255));

                this.renderables.Add(table);
                this.renderables.Add(lightSource);
            }

            // add renderables to scene
            CreateSceneWithCurrentRenderables();
        }


        private void CreateSceneWithCurrentRenderables()
        {
            this.viewPort.Children.Clear();

            foreach (var renderable in this.renderables)
            {
                this.viewPort.Children.Add(renderable);
            }
        }

        /// <summary>
        /// Remove the robot(s) from the scene
        /// </summary>
        public void RemoveRobotFromScene()
        {
            // get robot(s)
            var robots = this.renderables.Where(r => r is Robot).ToList();

            // remove robot(s)
            foreach (var robot in robots)
            {
                this.renderables.Remove(robot);
            }

            // rebuild scene
            CreateSceneWithCurrentRenderables();
        }

        /// <summary>
        /// Update robot in the scene
        /// </summary>
        /// <param name="robot"></param>
        public void UpdateRobotInScene(ToyRobotModel robot)
        {
            var table = this.renderables.FirstOrDefault(r => r is Table);
            var robots = this.renderables.Where(r => r is Robot).Select(r => r as Robot);
            var position = (table as Table).GetCenterOfTableGridReference((int)robot.Vector.Position.X, (int)robot.Vector.Position.Y);

            if (robots.Count() <= 0)
            {
                // add robot
                this.renderables.Add(new Robot() { Position = position, CompassDirection = robot.Vector.Dir });

                CreateSceneWithCurrentRenderables();
            }
            else
            {
                // update robot position & direction (no need to recreate the scene as no new renderable added)
                foreach (var r in robots)
                {
                    r.Position = position;
                    r.CompassDirection = robot.Vector.Dir;
                }
            }
        }
    }
}
