﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using ToyRobotChallenge.Library.Geometry;

namespace WPFToyRobotChallenge.UI.SceneRenderer.Models
{
    /// <summary>
    /// A renderable robot
    /// </summary>
    public class Robot
        : Renderable
    {
        private IDictionary<Direction, ImageBrush> imageBrushes = new Dictionary<Direction, ImageBrush>();
        private TranslateTransform3D translation = new TranslateTransform3D(0.0, 0.0, 0.0);
        private Direction direction = Direction.North;
        private DiffuseMaterial material;

        /// <summary>
        /// get / set the position of the robot in 3D space
        /// </summary>
        public Point3D Position
        { 
            get
            {
                return new Point3D(this.translation.OffsetX, this.translation.OffsetY, this.translation.OffsetZ);
            }
            set
            {
                translation.OffsetX = value.X;
                translation.OffsetY = value.Y;
                translation.OffsetZ = value.Z;
            }
        }

        /// <summary>
        /// get / set the robot compass direction
        /// </summary>
        public Direction CompassDirection
        {
            get => this.direction;
            set
            {
                if(this.direction != value)
                {
                    // update direction
                    this.direction = value;

                    // update material brush
                    this.material.Brush = imageBrushes[value];
                }
            }
        }

        public Robot()
        {
            CreateModel();
        }

        private void CreateModel()
        {
            /*
                <ModelVisual3D>
                    <ModelVisual3D.Content>
                        <Model3DGroup >
                            <Model3DGroup.Children>
                                <GeometryModel3D>
                                    <GeometryModel3D.Geometry>
                                        <MeshGeometry3D Positions= "0.0  0.0  0.0 
                                                                    0.0 -1.0  0.0 
                                                                    1.0 -1.0  0.0 
                                                                    0.0  0.0  0.0 
                                                                    1.0 -1.0  0.0 
                                                                    1.0  0.0  0.0"/>
                                    </GeometryModel3D.Geometry>
                                    <GeometryModel3D.Material>
                                        <DiffuseMaterial>
                                            <DiffuseMaterial.Brush>
                                                <SolidColorBrush 
                                                    Color="Red" 
                                                    Opacity="1.0"/>
                                            </DiffuseMaterial.Brush>
                                        </DiffuseMaterial>
                                    </GeometryModel3D.Material>
                                </GeometryModel3D>
                            </Model3DGroup.Children>
                        </Model3DGroup>
                    </ModelVisual3D.Content>
                </ModelVisual3D>
             */

            // load image
            LoadCompassDirectionImages();
            
            this.material = new DiffuseMaterial(this.imageBrushes[this.CompassDirection]);

            // create model
            this.model = new ModelVisual3D()
            {
                Content = new Model3DGroup()
                {
                    Children = new Model3DCollection(
                                    new Model3D[]
                                    {
                                        new GeometryModel3D()
                                        {
                                            Geometry =  new MeshGeometry3D()
                                                        {
                                                            Positions = new Point3DCollection(
                                                                            new Point3D[]
                                                                            {
                                                                                    new Point3D(-0.5,  0.5, 0.0), // 0
                                                                                    new Point3D(-0.5, -0.5, 0.0), // 1
                                                                                    new Point3D( 0.5, -0.5, 0.0), // 2
                                                                                    new Point3D( 0.5,  0.5, 0.0), // 3
                                                                            }),
                                                            TextureCoordinates = new PointCollection(
                                                                                    new Point[]
                                                                                    {
                                                                                        new Point(0, 0),        // 0
                                                                                        new Point(0, 1),        // 1
                                                                                        new Point(1, 1),        // 2
                                                                                        new Point(1, 0),        // 3
                                                                                    }),
                                                            TriangleIndices = new Int32Collection(
                                                                                new int[]
                                                                                {
                                                                                    0, 1, 2,
                                                                                    0, 2, 3
                                                                                }),

                                            },
                                            Material = this.material,
                                            Transform = new Transform3DGroup()
                                            {
                                                Children = new Transform3DCollection(
                                                            new Transform3D[]
                                                            {
                                                                this.translation
                                                            }),
                                            }
                                        }
                                    })
                }
            };
        }

        private void LoadCompassDirectionImages()
        {
            var paths = new Dictionary<Direction, Uri>
            {
                [Direction.North] = new Uri(@"pack://application:,,,/UI/Textures/bacterium_back.png",  UriKind.Absolute),
                [Direction.East]  = new Uri(@"pack://application:,,,/UI/Textures/bacterium_right.png", UriKind.Absolute),
                [Direction.South] = new Uri(@"pack://application:,,,/UI/Textures/bacterium_front.png", UriKind.Absolute),
                [Direction.West]  = new Uri(@"pack://application:,,,/UI/Textures/bacterium_left.png",  UriKind.Absolute),
            };

            foreach(var kv in paths)
            {
                var image = new BitmapImage();

                image.BeginInit();
                {
                    image.UriSource = kv.Value;
                    image.CacheOption = BitmapCacheOption.OnLoad;
                }
                image.EndInit();

                this.imageBrushes[kv.Key] = new ImageBrush(image);
            }
        }
    }
}
