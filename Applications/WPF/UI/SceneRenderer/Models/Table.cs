﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace WPFToyRobotChallenge.UI.SceneRenderer.Models
{
    /// <summary>
    /// A Renderable table
    /// </summary>
    public class Table
        : Renderable
    {
        private int height;
        private int width;
        private int colCount;
        private int rowCount;

        public Table(int width, int height, int colCount, int rowCount)
        {
            this.height = height;
            this.width = width;
            this.colCount = colCount;
            this.rowCount = rowCount;

            this.CreateModel();
        }

        private void CreateModel()
        {
            /*
                <ModelVisual3D>
                    <ModelVisual3D.Content>
                        <Model3DGroup >
                            <Model3DGroup.Children>
                                <GeometryModel3D>
                                    <GeometryModel3D.Geometry>
                                        <MeshGeometry3D Positions="-1.5 1.0 0.0  0.0 -1.0 0.0 1.5 1.0 0"/>
                                    </GeometryModel3D.Geometry>
                                    <GeometryModel3D.Material>
                                        <DiffuseMaterial>
                                            <DiffuseMaterial.Brush>
                                                <SolidColorBrush 
                                                    Color="Red" 
                                                    Opacity="1.0"/>
                                            </DiffuseMaterial.Brush>
                                        </DiffuseMaterial>
                                    </GeometryModel3D.Material>
                                </GeometryModel3D>
                            </Model3DGroup.Children>
                        </Model3DGroup>
                    </ModelVisual3D.Content>
                </ModelVisual3D>
             */

            var meshGeometry = new MeshGeometry3D()
            {
                Positions = new Point3DCollection(
                                    new Point3D[]
                                    {
                                        new Point3D(-0.5,  0.5, 0.0),
                                        new Point3D(-0.5, -0.5, 0.0),
                                        new Point3D( 0.5, -0.5, 0.0),
                                        new Point3D( 0.5,  0.5, 0.0),
                                    }),
                TriangleIndices = new Int32Collection(
                                        new int[]
                                        {
                                            0, 1, 2,
                                            0, 2, 3
                                        }),
            };

            var meshMaterial = new DiffuseMaterial(new SolidColorBrush(Color.FromRgb(0, 155, 0)));
            var meshScaleV = new ScaleTransform3D(0.01, this.height, 1.0);
            var meshScaleH = new ScaleTransform3D(this.width, 0.01, 1.0);
            var colWidth = (double)this.width / (double)this.colCount;
            var rowHeight = (double)this.height / (double)this.rowCount;
            var models = new List<Model3D>(this.colCount - 1);

            // draw vertical grid lines
            for(var c = 0;  c < (this.colCount - 1); c++)
            {
                var offsetX = -((double)this.width / 2.0) + (c + 1) * colWidth;

                models.Add(
                    new GeometryModel3D()
                    {
                        Geometry = meshGeometry,
                        Material = meshMaterial,
                        Transform = new Transform3DGroup()
                        {
                            Children = new Transform3DCollection(
                                        new Transform3D[]
                                        {
                                            meshScaleV,
                                            new TranslateTransform3D(offsetX, 0.0, 0.0)
                                        }),
                        }
                    });
            };

            // draw horizontal grid lines
            for(var r = 0; r < (this.rowCount - 1); r++)
            {
                var offsetY = -((double)this.height / 2.0) + (r + 1) * rowHeight;

                models.Add(
                    new GeometryModel3D()
                    {
                        Geometry = meshGeometry,
                        Material = meshMaterial,
                        Transform = new Transform3DGroup()
                        {
                            Children = new Transform3DCollection(
                                        new Transform3D[]
                                        {
                                            meshScaleH,
                                            new TranslateTransform3D(0.0, offsetY, 0.0)
                                        }),
                        }
                    });
            }

            // draw grid coordinates
            for (var c = 0; c < this.colCount; c++)
            {
                for (var r = 0; r < this.rowCount; r++)
                {
                    // create and add label to render grid coordinates
                    var labelModel = CreateTextLabel3D($"({c},{r})",
                                                       new SolidColorBrush(Color.FromRgb(0, 155, 0)),
                                                       ((double)this.height / (double)this.rowCount / 2.0),
                                                       this.GetCenterOfTableGridReference(c, r),
                                                       new Vector3D(0.2, 0.0, 0.0),
                                                       new Vector3D(0.0, 0.2, 0.0));

                    models.Add(labelModel);
                }
            }

            this.model = new ModelVisual3D()
            {
                Content = new Model3DGroup()
                {
                    Children = new Model3DCollection(models)
                }
            };
        }

        /// <summary>
        /// For a given grid reference (col, row) return the center position in 3D spce of that grid reference
        /// </summary>
        /// <param name="col"></param>
        /// <param name="row"></param>
        /// <returns></returns>
        public Point3D GetCenterOfTableGridReference(int col, int row)
        {
            var colWidth = (double)this.width / (double)this.colCount;
            var rowHeight = (double)this.height / (double)this.rowCount;

            var offsetX = -((double)this.width / 2.0) + ((col + 0.5) * colWidth);
            var offsetY = -((double)this.height / 2.0) + ((row + 0.5) * rowHeight);

            return new Point3D(offsetX, offsetY, 0.0);
        }

        /// <summary>
        /// Based on code found here: https://ericsink.com/wpf3d/4_Text.html
        /// </summary>
        /// <param name="text"></param>
        /// <param name="textColor"></param>
        /// <param name="bDoubleSided"></param>
        /// <param name="height"></param>
        /// <param name="center"></param>
        /// <param name="over"></param>
        /// <param name="up"></param>
        /// <returns></returns>
        private static GeometryModel3D CreateTextLabel3D(string text,
                                                         Brush textColor,
                                                         double height,
                                                         Point3D center,
                                                         Vector3D over,
                                                         Vector3D up)
        {
            var mat = new DiffuseMaterial()
            {
                Brush = new VisualBrush(
                                new TextBlock(new Run(text))
                                {
                                    Foreground = textColor,
                                    FontFamily = new FontFamily("Arial"),
                                })
            };

            // build the geometry for the label (two adjacent triangles forming a rectangle)
            var mg = new MeshGeometry3D()
            {
                Positions = new Point3DCollection(
                                    new Point3D[]
                                    {
                                        (center - ((text.Length * height) / 2) * over) - ((height / 2) * up),       // left/bottom  // 0 
                                        (center - ((text.Length * height) / 2) * over) + ((height / 2) * up),       // left/top     // 1
                                        (center + ((text.Length * height) / 2) * over) - ((height / 2) * up),       // right/bottom // 2
                                        (center + ((text.Length * height) / 2) * over) + ((height / 2) * up)        // right/top    // 3
                                    }),

                TextureCoordinates = new PointCollection(
                                        new Point[]
                                        {
                                            new Point(0, 1),
                                            new Point(0, 0),
                                            new Point(1, 1),
                                            new Point(1, 0),
                                        }),

                TriangleIndices = new Int32Collection(
                                        new int[]
                                        {
                                            0, 3, 1,
                                            0, 2, 3
                                        }),
            };

            // return model from created mesh and material
            return new GeometryModel3D(mg, mat);
        }
    }
}
