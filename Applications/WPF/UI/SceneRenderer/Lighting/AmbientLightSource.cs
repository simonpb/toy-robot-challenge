﻿using System.Windows.Media;
using System.Windows.Media.Media3D;

namespace WPFToyRobotChallenge.UI.SceneRenderer.Lighting
{
    class AmbientLightSource
        : Renderable
    {
        private Color color;

        public AmbientLightSource(Color color)
        {
            this.color = color;

            CreateModel();
        }

        private void CreateModel()
        {
            /*
               <ModelVisual3D>
                    <ModelVisual3D.Content>
                        <Model3DGroup >
                            <Model3DGroup.Children>
                                <AmbientLight Color="#FFFFFFFF" />
                            </Model3DGroup.Children>
                        </Model3DGroup>
                    </ModelVisual3D.Content>
                </ModelVisual3D>* 
             */

            this.model = new ModelVisual3D()
            {
                Content = new Model3DGroup()
                {
                    Children = new Model3DCollection(
                                    new Model3D[]
                                    {
                                        new AmbientLight(this.color)
                                    })
                }
            };
        }
    }
}
