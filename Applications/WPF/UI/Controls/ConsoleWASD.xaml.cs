﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace WPFToyRobotChallenge.UI.Controls
{

    /// <summary>
    /// Interaction logic for ConsoleWASD.xaml
    /// </summary>
    public partial class ConsoleWASD : UserControl
    {
        public enum ViewMode
        {
            PLACE,
            WASD
        }

        public class ConsoleWASDCommandEvent
            : EventArgs
        {
            public string Command { get; }

            public ConsoleWASDCommandEvent(string command)
            {
                this.Command = command;
            }
        }

        public event EventHandler OnCommand;

        private ViewMode mode = ViewMode.PLACE;

        public ViewMode Mode
        {
            get
            {
                return this.mode;
            }
            set
            {
                this.mode = value;

                switch (value)
                {
                    case ViewMode.PLACE:
                        this.place.Visibility = Visibility.Visible;
                        this.wasd.Visibility = Visibility.Hidden;
                        break;
                    case ViewMode.WASD:
                        this.place.Visibility = Visibility.Hidden;
                        this.wasd.Visibility = Visibility.Visible;
                        break;
                }
            }
        }

        public string X { get; set; }
        public string Y { get; set; }
        public string Direction { get; set; }


        public bool IsPlaceButtonEnabled
        {
            get
            {
                return (true
                        && int.TryParse(this.X, out var x)
                        && int.TryParse(this.Y, out var y)
                        && Enum.TryParse<ToyRobotChallenge.Library.Geometry.Direction>(this.Direction, out var direction));
            }
        }

        public ConsoleWASD()
        {
            InitializeComponent();

            // Set binding context to THIS
            this.DataContext = this;
        }

        private void OnPlace_Clicked(object sender, RoutedEventArgs e)
        {
            this.OnCommand?.Invoke(this, new ConsoleWASDCommandEvent($"PLACE {this.X ?? ""},{this.Y ?? ""}, {this.Direction?.ToUpper() ?? ""}"));
        }

        private void OnLeft_Clicked(object sender, RoutedEventArgs e)
        {
            this.OnCommand?.Invoke(this, new ConsoleWASDCommandEvent($"LEFT"));
        }

        private void OnMove_Clicked(object sender, RoutedEventArgs e)
        {
            this.OnCommand?.Invoke(this, new ConsoleWASDCommandEvent($"MOVE"));
        }

        private void OnRight_Clicked(object sender, RoutedEventArgs e)
        {
            this.OnCommand?.Invoke(this, new ConsoleWASDCommandEvent($"RIGHT"));
        }

        private void OnReset_Clicked(object sender, RoutedEventArgs e)
        {
            this.OnCommand?.Invoke(this, new ConsoleWASDCommandEvent($"QUIT"));
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.placeButton.IsEnabled = this.IsPlaceButtonEnabled;
        }

        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            this.placeButton.IsEnabled = this.IsPlaceButtonEnabled;
        }
    }
}
