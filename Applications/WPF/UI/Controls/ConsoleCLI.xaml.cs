﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Input;

namespace WPFToyRobotChallenge.UI.Controls
{
    /// <summary>
    /// Interaction logic for ConsoleCLI.xaml
    /// </summary>
    public partial class ConsoleCLI : UserControl
    {
        public class ConsoleCLICommandEvent
            : EventArgs
        {
            public string Command { get; }

            public ConsoleCLICommandEvent(string command)
            {
                this.Command = command;
            }            
        }

        public event EventHandler OnCommand;

        private IList<string> commandHistory = new List<string>();
        public int MaxCommands { get; set; } = 5;

        public ConsoleCLI()
        {
            InitializeComponent();
        }

        private void onCommandKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                var cmd = this.commandTextBox.Text.Trim();

                if (string.IsNullOrWhiteSpace(cmd) == false)
                {
                    this.commandHistory.Add(cmd);
                    this.OnCommand?.Invoke(this, new ConsoleCLICommandEvent(cmd));
                }

                while (this.commandHistory.Count > this.MaxCommands)
                {
                    this.commandHistory.RemoveAt(0);
                }

                this.commandHistoryText.Text = string.Join("\n", this.commandHistory);
                this.commandTextBox.Text = string.Empty;
            }
        }
    }
}
