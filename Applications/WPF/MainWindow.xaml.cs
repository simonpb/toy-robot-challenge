﻿using MediatR;
using StructureMap;
using System.Threading.Tasks;
using System.Windows;
using ToyRobotChallenge.Library.AppSupport.Application.Implementations;
using ToyRobotChallenge.Library.AppSupport.Application.Interfaces;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Handlers;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using WPFToyRobotChallenge.Controllers;
using WPFToyRobotChallenge.UI.SceneRenderer;
using static ToyRobotChallenge.Library.AppSupport.Models.ToyRobotModel;
using static WPFToyRobotChallenge.UI.Controls.ConsoleCLI;
using static WPFToyRobotChallenge.UI.Controls.ConsoleWASD;

namespace WPFToyRobotChallenge
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        
        private Container serviceContainer = null;

        public MainWindow()
        {
            InitializeComponent();
            InitialiseContainer();
            InitialiseScene();

            StartSimulatorAsync()
                .ConfigureAwait(false)
                .GetAwaiter()
                .GetResult();
        }
    }

    /// <summary>
    /// Scene renderer
    /// </summary>
    public partial class MainWindow
    {
        private RenderEngine renderEngine;

        private void InitialiseScene()
        {
            this.renderEngine = new RenderEngine(this.viewPort);
        }
    }

    /// <summary>
    /// Service container initialisation
    /// </summary>
    public partial class MainWindow
    {
        private void InitialiseContainer()
        {
            var container = new Container(cfg =>
            {
                // add mediatR handlers
                cfg.For<StartHandler>().Use<StartHandler>();
                cfg.For<ResetHandler>().Use<ResetHandler>();
                cfg.For<ExecuteHandler>().Use<ExecuteHandler>();

                // one and only simulator
                cfg.For<IToyRobotSimulator>().Singleton().Use<StockToyRobotSimulator>();

                // add service factory, mediator & controller
                cfg.For<ServiceFactory>().Use<ServiceFactory>(ctx => ctx.GetInstance);
                cfg.For<IMediator>().Use<Mediator>();
                cfg.For<ToyRobotController>().Use<ToyRobotController>();
            });

            this.serviceContainer = container;
        }
    }

    /// <summary>
    /// Simulation execution
    /// </summary>
    public partial class MainWindow
    {
        private async Task StartSimulatorAsync()
        {
            var controller = this.serviceContainer.GetInstance<ToyRobotController>();

            await controller.StartSimulationAsync(5, 5);
        }

        private async void ConsoleCLI_OnCommand(object sender, System.EventArgs e)
        {
            if ((e is ConsoleCLICommandEvent) == true)
            {
                await ExecuteCommand((e as ConsoleCLICommandEvent).Command);
            }
        }

        private async void ConsoleWASD_OnCommand(object sender, System.EventArgs e)
        {
            if ((e is ConsoleWASDCommandEvent) == true)
            {
                await ExecuteCommand((e as ConsoleWASDCommandEvent).Command);
            }
        }

        private async Task ExecuteCommand(string command)
        {
            var controller = this.serviceContainer.GetInstance<ToyRobotController>();
            var robot = await controller.ExecuteAsync(command);

            if (true
                && (robot?.Vector?.IsRobotPlaced() ?? false) == true
                && (robot?.State ?? RobotState.None) != RobotState.None)
            {
                this.renderEngine.UpdateRobotInScene(robot);
                this.wasd.Mode = ViewMode.WASD;
            }
            else
            {
                this.renderEngine.RemoveRobotFromScene();
                this.wasd.Mode = ViewMode.PLACE;
            }
        }
    }
}
