﻿using MediatR;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Models;
using static ToyRobotChallenge.Library.Simulator;

namespace BlazorToyRobotChallenge.Controllers
{
    public interface IToyRobotController
    {
        Task<ToyRobotModel> ExecuteAsync(IRobotCommand command);
        Task<ToyRobotModel> ExecuteAsync(IEnumerable<IRobotCommand> commands);
        Task<ToyRobotModel> ResetSimulationAsync(ulong rows, ulong columns);
        Task<ToyRobotModel> StartSimulationAsync(ulong rows, ulong columns);
    }

    public class ToyRobotController : IToyRobotController
    {
        private readonly IMediator mediatR;

        public ToyRobotController(IMediator mediatR)
        {
            this.mediatR = mediatR;
        }

        public async Task<ToyRobotModel> ExecuteAsync(IRobotCommand command)
        {
            return await mediatR.Send(new Execute(command));
        }

        public async Task<ToyRobotModel> ExecuteAsync(IEnumerable<IRobotCommand> commands)
        {
            return await mediatR.Send(new Execute(commands));
        }

        public async Task<ToyRobotModel> StartSimulationAsync(ulong rows, ulong columns)
        {
            return await mediatR.Send(new Start(rows, columns));
        }

        public async Task<ToyRobotModel> ResetSimulationAsync(ulong rows, ulong columns)
        {
            return await mediatR.Send(new Reset(rows, columns));
        }
    }
}
