﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using ToyRobotChallenge.Library;

namespace BlazorToyRobotChallenge.Shared
{
    public partial class CommandConsoleWASD
    {
        [Parameter]
        public EventCallback<Simulator.IRobotCommand> OnCommandEvent { get; set; }

        [Parameter]
        public EventCallback OnCommandReset { get; set; }

        private async Task OnRotateLeftAsync()
        {
            // raise "turn left" command
            await OnCommandEvent.InvokeAsync(new Simulator.LeftTurnCommand());
        }

        private async Task OnRotateRightAsync()
        {
            // raise "turn right" command
            await OnCommandEvent.InvokeAsync(new Simulator.RightTurnCommand());
        }

        private async Task OnMoveAsync()
        {
            // raise "move" command
            await OnCommandEvent.InvokeAsync(new Simulator.MoveCommand());
        }

        private async Task OnResetAsync()
        {
            // raise "reset" command
            await OnCommandReset.InvokeAsync();
        }
    }
}
