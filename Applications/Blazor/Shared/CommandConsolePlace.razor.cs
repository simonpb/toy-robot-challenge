﻿using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using ToyRobotChallenge.Library;
using ToyRobotChallenge.Library.Geometry;

namespace BlazorToyRobotChallenge.Shared
{
    public partial class CommandConsolePlace
    {
        private int? X;
        private int? Y;
        private string direction = string.Empty;

        [Parameter]
        public int MaxX { set; private get; } = 5;

        [Parameter]
        public int MaxY { set; private get; } = 5;

        [Parameter]
        public EventCallback<Simulator.IRobotCommand> OnCommandEvent { get; set; }

        private bool IsPlaceButtonDisabled => (this.X.HasValue && this.Y.HasValue && this.X >= 0 && this.X < this.MaxX && this.Y >= 0 && this.Y < this.MaxY && System.Enum.TryParse(typeof(Direction), this.direction, out var d) == true) == false;

        private async Task OnPlaceAsync()
        {
            if (this.IsPlaceButtonDisabled == false)
            {
                // raise "place" command
                var dir = (Direction)System.Enum.Parse(typeof(Direction), this.direction);
                var command = new Simulator.PlaceCommand(new Vector2d<ulong>(new Point2d<ulong>((ulong)this.X, (ulong)this.Y), dir));

                await OnCommandEvent.InvokeAsync(command);
            }
        }
    }
}
