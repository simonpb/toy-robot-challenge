﻿using BlazorToyRobotChallenge.Controllers;
using Microsoft.AspNetCore.Components;
using System.Threading.Tasks;
using ToyRobotChallenge.Library;
using ToyRobotChallenge.Library.AppSupport.Models;
using ToyRobotChallenge.Library.AppSupport.Geometry;

namespace BlazorToyRobotChallenge.Shared
{
    public partial class SimulatorWASD
    {
        private readonly int rows = 5;
        private readonly int columns = 5;

        private ToyRobotModel RobotModel { get; set; }
        private bool RobotIsPlaced => (this.RobotModel?.Vector is null) == false;
        private int RobotHeight => 55; // 32;
        private int RobotWidth => 55; // 50;

        [Inject]
        protected IToyRobotController ToyRobotController { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.RobotModel = await ToyRobotController.StartSimulationAsync((ulong)this.rows, (ulong)this.columns);
        }
    }

    /// <summary>
    /// Event handlers
    /// </summary>
    public partial class SimulatorWASD
    { 
        private async Task OnConsoleCommandResetAsync()
        {
            this.RobotModel = await ToyRobotController.ResetSimulationAsync((ulong)this.rows, (ulong)this.columns);
        }

        private async Task OnConsoleCommandAsync(Simulator.IRobotCommand command)
        {
            this.RobotModel = await ToyRobotController.ExecuteAsync(command);
        }
    }
}
