﻿using BlazorToyRobotChallenge.Controllers;
using Microsoft.AspNetCore.Components;
using System.Linq;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Models;
using ToyRobotChallenge.Library.AppSupport.Application.Implementations.ExtraRobotCommands;
using ToyRobotChallenge.Library.AppSupport.UserCommands;
using static ToyRobotChallenge.Library.Simulator;

namespace BlazorToyRobotChallenge.Shared
{
    public partial class SimulatorCLI
    {
        private readonly int rows = 5;
        private readonly int columns = 5;
        private CommandConsoleCLI commandConsole = null;

        private ToyRobotModel RobotModel { get; set; }

        [Inject]
        protected IToyRobotController ToyRobotController { get; set; }

        protected override async Task OnInitializedAsync()
        {
            this.RobotModel = await this.ToyRobotController.StartSimulationAsync((ulong)this.rows, (ulong)this.columns);
        }
    }

    /// <summary>
    /// Event handlers
    /// </summary>
    public partial class SimulatorCLI
    { 
        private async Task OnConsoleCommandAsync(string command)
        {
            await ExecuteRobotCommandAsync(command);
        }
    }

    /// <summary>
    /// Support methods
    /// </summary>
    public partial class SimulatorCLI
    { 
        private async Task ExecuteRobotCommandAsync(string command)
        {
            var commandList = new CommandList(command, true);

            if (commandList.Count() > 0)
            {
                foreach (var cmd in commandList)
                {
                    // commands handled at the application level
                    if (cmd is QuitCommand)
                    {
                        await ExecuteSimulationQuitAsync();
                    }
                    else if (cmd is HelpCommand)
                    {
                        await ExecuteDisplayHelpTextAsync();
                    }
                    else if (cmd is ReportCommand)
                    {
                        await ExecuteDisplayRobotReportAsync();
                    }
                    else
                    {
                        // command handled else where
                        this.RobotModel = await this.ToyRobotController.ExecuteAsync(cmd);
                    }
                }
            }
            else
            {
                // can't parse command in to command list. display a helpful hint
                commandConsole.WriteLines(new string[] { "type h <enter> to display help, type q <enter> to reset the simulation" });
            }
        }

        private async Task ExecuteSimulationQuitAsync()
        {
            this.RobotModel = await this.ToyRobotController.ResetSimulationAsync((ulong)this.rows, (ulong)this.columns);
        }

        private Task ExecuteDisplayRobotReportAsync()
        {
            if (this.RobotModel is null)
            {
                commandConsole.WriteLines(new string[] { "Robot MUST be placed first!" });
            }
            else
            {
                commandConsole.WriteLines(new string[] { $"Position: ({this.RobotModel.Vector.Position.X}, {this.RobotModel.Vector.Position.Y}) Direction: {this.RobotModel.Vector.Dir}" });
            }

            return Task.CompletedTask;
        }

        private Task ExecuteDisplayHelpTextAsync()
        {
            if (commandConsole != null)
            {
                var lines = new string[]
                {
                    "Use the following commands to ready & control your robot (commands are case insensitive!):",
                    "PLACE [x],[y], [FACING]\" - Place the robot in the required x,y position in the required direction.",
                    $"x,y are the grid coordinates 0 -> {this.columns - 1} for width & 0->{this.rows - 1} for height.",
                    "    Facing is one of NORTH, EAST, SOUTH, WEST",
                    "You must place your robot first, any other interactive commands will be ignored until the robot has been placed.",
                    "You may place your robot as many tmes as you like.",
                    "MOVE - Move the robot by 1 grid square in the direction it is facing.",
                    "LEFT - Rotate the robot 90 degrees left in the current grid position.",
                    "RIGHT - Rotate the robot 90 degrees right in the current grid position.",
                    "REPORT - Display the robots current position & direction.",
                    "WAVE - Give a little wave :-)",
                    "JUMP - Give a little jump :-P"
                };

                commandConsole.WriteLines(lines);
            }

            return Task.CompletedTask;
        }
    }
}
