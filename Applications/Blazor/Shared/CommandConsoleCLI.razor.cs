﻿using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Web;
using System.Collections.Generic;
using System.Threading.Tasks;
using MightyG.Collections;
using Microsoft.JSInterop;

namespace BlazorToyRobotChallenge.Shared
{
    public partial class CommandConsoleCLI
    {
        private readonly string commandPrompt = "> ";
        private readonly string standardEnterCode = "Enter";
        private readonly string numpadEnterCode = "NumpadEnter";

        private string commandBuffer = string.Empty;
        private FixedLengthList<string> commands = new FixedLengthList<string>(12);

        public void WriteLines(IEnumerable<string> lines)
        {
            commands.Add(lines);
        }

        [Parameter]
        public EventCallback<string> OnCommandEvent { get; set; }

        [Inject]
        private IJSRuntime JsRuntime { get; set; }

        private async Task OnCommandKeyPressAsync(KeyboardEventArgs args)
        {
            // await JsRuntime.InvokeVoidAsync("alert", $"{System.Text.Json.JsonSerializer.Serialize(args)}"); // Alert


            if (false
                || args.Key == standardEnterCode
                || args.Key == numpadEnterCode
                || args.Code == standardEnterCode 
                || args.Code == numpadEnterCode)
            {
                // add command and raise event
                commands.Add(commandBuffer);

                // clear
                commandBuffer = string.Empty;

                // raise "on command" event
                await OnCommandEvent.InvokeAsync(commands[commands.Count - 1]);
            }
        }
    }
}
