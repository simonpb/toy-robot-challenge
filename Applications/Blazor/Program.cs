using BlazorToyRobotChallenge;
using BlazorToyRobotChallenge.Controllers;
using MediatR;
using Microsoft.AspNetCore.Components.Web;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using ToyRobotChallenge.Library.AppSupport.Application.Implementations;
using ToyRobotChallenge.Library.AppSupport.Application.Interfaces;

var builder = WebAssemblyHostBuilder.CreateDefault(args);

builder.RootComponents.Add<App>("#app");
builder.RootComponents.Add<HeadOutlet>("head::after");

builder.Services.AddScoped(sp => new HttpClient { BaseAddress = new Uri(builder.HostEnvironment.BaseAddress) });
builder.Services.AddScoped<IToyRobotController, ToyRobotController>();
builder.Services.AddSingleton<IToyRobotSimulator>(sp => new StockToyRobotSimulator());
builder.Services.AddSingleton<SemVersion.SemanticVersion>(sp => new SemVersion.SemanticVersion(1, 1, 0, "alpha", ""));
builder.Services.AddMediatR(System.Reflection.Assembly.GetAssembly(typeof(StockToyRobotSimulator)));

await builder.Build().RunAsync();
