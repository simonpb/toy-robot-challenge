﻿using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace MVCToyRobotChallenge.Controllers
{
    public class ToyRobotController : Controller
    {
        protected readonly int _tableWidth = 5;
        protected readonly int _tableHeight = 5;
        protected readonly IMediator _mediatR;

        public ToyRobotController(IMediator mediatR)
        {
            this._mediatR = mediatR;
        }
    }
}
