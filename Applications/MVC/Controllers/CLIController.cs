﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCToyRobotChallenge.Models;
using MVCToyRobotChallenge.Services;
using System.Linq;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.Implementations.ExtraRobotCommands;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using ToyRobotChallenge.Library.AppSupport.UserCommands;
using static ToyRobotChallenge.Library.Simulator;

namespace MVCToyRobotChallenge.Controllers
{
    public partial class CLIController
        : ToyRobotController
    {
        private readonly ILogger<CLIController> _logger;
        private readonly ICommandHistory _commandHistory;

        public CLIController(IMediator mediatR, ICommandHistory commandHistory, ILogger<CLIController> logger)
            : base(mediatR)
        {
            _logger = logger;
            _commandHistory = commandHistory;
        }
    }

    /// <summary>
    /// Endpoints
    /// </summary>
    public partial class CLIController
    { 
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await this._mediatR.Send(new Start((ulong)_tableHeight, (ulong)_tableWidth));

            // return model
            return View(await CreateCLIModelAsync());
        }

        [HttpPost]
        public async Task<IActionResult> Index(ConsoleCLIModel input)
        {
            var command = input.CommandBuffer;

            // add command to command history
            this._commandHistory.Push(command);

            await ExecuteRobotCommandAsync(command);

            ModelState.Clear();

            return View(await CreateCLIModelAsync());
        }
    }

    /// <summary>
    /// Support
    /// </summary>
    public partial class CLIController
    { 
        private async Task ExecuteRobotCommandAsync(string command)
        {
            var commandList = new CommandList(command ?? string.Empty, true);

            if (commandList.Count() > 0)
            {
                foreach(var cmd in commandList)
                {
                    // commands handled at the application level
                    if(cmd is QuitCommand)
                    {
                        await ExecuteSimulationQuitAsync();
                    }
                    else if (cmd is HelpCommand)
                    {
                        await ExecuteDisplayHelpTextAsync();
                    }
                    else if(cmd is ReportCommand)
                    {
                        await ExecuteDisplayRobotReportAsync();
                    }
                    else
                    {
                        // command handled else where
                        await this._mediatR.Send(new Execute(cmd));
                    }
                }
            }
            else
            {
                // can't parse command in to command list. display a helpful hint
                this._commandHistory.Push("type h <enter> to display help, type q <enter> to reset the simulation");
            }
        }

        private async Task ExecuteSimulationQuitAsync()
        {
            await this._mediatR.Send(new Reset((ulong)_tableHeight, (ulong)_tableWidth));
        }

        private async Task ExecuteDisplayRobotReportAsync()
        {
            var robot = await this._mediatR.Send(new Execute(new ReportCommand()));

            if ((robot?.Vector?.IsRobotPlaced() ?? false) == false)
            {
                this._commandHistory.Push("Robot MUST be placed first!");
            }
            else
            {
                this._commandHistory.Push($"Position: ({robot.Vector.Position.X}, {robot.Vector.Position.Y}) Direction: {robot.Vector.Dir}");
            }
        }

        private Task ExecuteDisplayHelpTextAsync()
        {
            var lines = new string[]
            {
                "Use the following commands to ready & control your robot (commands are case insensitive!):",
                "PLACE [x],[y], [FACING]\" - Place the robot in the required x,y position in the required direction.",
                $"x,y are the grid coordinates 0 -> {this._tableWidth - 1} for width & 0->{this._tableHeight - 1} for height.",
                "    Facing is one of NORTH, EAST, SOUTH, WEST",
                "You must place your robot first, any other interactive commands will be ignored until the robot has been placed.",
                "You may place your robot as many tmes as you like.",
                "MOVE - Move the robot by 1 grid square in the direction it is facing.",
                "LEFT - Rotate the robot 90 degrees left in the current grid position.",
                "RIGHT - Rotate the robot 90 degrees right in the current grid position.",
                "REPORT - Display the robots current position & direction.",
                "WAVE - Give a little wave :-)",
                "JUMP - Give a little jump :-P"
            };

            this._commandHistory.Push(lines);

            return Task.CompletedTask;
        }

        private async Task<CLIModel> CreateCLIModelAsync()
        {
            var robot = await this._mediatR.Send(new Execute(new ReportCommand()));
            var robotModel = robot is null
                                ? null
                                : new ToyRobotModel(robot);

            var tableTop = new TabletopModel()
            {
                Rows = _tableHeight,
                Columns = _tableWidth,
                RobotModel = robotModel,
            };

            var console = new ConsoleCLIModel()
            {
                Commands = this._commandHistory.Commands,
                CommandBuffer = string.Empty,
            };

            var model = new CLIModel()
            {
                TabletopModel = tableTop,
                ConsoleModel = console,
            };

            return model;
        }
    }
}
