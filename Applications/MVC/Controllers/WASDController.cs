﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MVCToyRobotChallenge.Models;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using ToyRobotChallenge.Library.Geometry;
using static ToyRobotChallenge.Library.Simulator;


namespace MVCToyRobotChallenge.Controllers
{
    public partial class WASDController
        : ToyRobotController
    {
        private readonly ILogger<WASDController> _logger;

        public WASDController(IMediator mediatR, ILogger<WASDController> logger)
            : base(mediatR)
        {
            _logger = logger;
        }
    }

    /// <summary>
    /// Endpoints
    /// </summary>
    public partial class WASDController
    {
        [HttpGet]
        public async Task<IActionResult> Index()
        {
            await this._mediatR.Send(new Start((ulong)_tableHeight, (ulong)_tableWidth));

            // return model
            return View(await CreateWASDModelAsync());
        }

        [ActionName("TurnLeft")]
        public async Task<IActionResult> TurnLeft()
        {
            await this._mediatR.Send(new Execute(new LeftTurnCommand()));

            // return model
            return View("Index", await CreateWASDModelAsync());
        }

        [ActionName("TurnRight")]
        public async Task<IActionResult> TurnRight()
        {
            await this._mediatR.Send(new Execute(new RightTurnCommand()));

            // return model
            return View("Index", await CreateWASDModelAsync());
        }

        [ActionName("Move")]
        public async Task<IActionResult> Move()
        {
            await this._mediatR.Send(new Execute(new MoveCommand()));

            // return model
            return View("Index", await CreateWASDModelAsync());
        }

        [ActionName("Reset")]
        public async Task<IActionResult> Reset()
        {
            await this._mediatR.Send(new Reset((ulong)_tableHeight, (ulong)_tableWidth));

            // return model
            return View("Index", await CreateWASDModelAsync());
        }

        [ActionName("Place")]
        public async Task<IActionResult> Place(ConsoleWASDModel model)
        {
            model.RobotModel.Vector.Dir = System.Enum.TryParse<Direction>(model.Direction, out var d) == true
                                            ? d
                                            : Direction.North;

            await this._mediatR.Send(new Execute(new PlaceCommand(model.RobotModel.Vector.ToULong())));

            // return model
            return View("Index", await CreateWASDModelAsync());
        }
    }

    /// <summary>
    /// Support
    /// </summary>
    public partial class WASDController
    { 
        private async Task<WASDModel> CreateWASDModelAsync()
        {
            var robot = await this._mediatR.Send(new Execute(new ReportCommand()));
            var robotModel = robot is null
                                ? null
                                : new ToyRobotModel(robot);

            var tableTop = new TabletopModel()
            {
                Rows = _tableHeight,
                Columns = _tableWidth,
                RobotModel = robotModel,
            };
           
            var console = new ConsoleWASDModel()
            {
                RobotModel = robotModel,
            };

            var model = new WASDModel()
            {
                TabletopModel = tableTop,
                ConsoleModel = console,
            };

            return model;
        }
    }
}
