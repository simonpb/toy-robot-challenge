﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using MightyG.Collections;

namespace MVCToyRobotChallenge.Services
{
    public interface ICommandHistory
    {
        IEnumerable<string> Commands { get; }

        void Push(string command);
        void Push(IEnumerable<string> lines);
    }

    public class CommandHistory 
        : ICommandHistory
    {
        private static int _maxCommandsCount = 12;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        private FixedLengthList<string> _commands = new FixedLengthList<string>(_maxCommandsCount);

        public IEnumerable<string> Commands => _commands;

        public CommandHistory(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._session = _httpContextAccessor.HttpContext.Session;

            // deserialise data from session...
            DeserialiseFromSession();
        }

        public void Push(string command)
        {
            _commands.Add(command);

            SerialiseToSession();
        }

        private void SerialiseToSession()
        {
            // serialize as a regular list of strings
             this._session.SetString(typeof(CommandHistory).FullName, System.Text.Json.JsonSerializer.Serialize(new List<string>(this._commands)));
        }

        private void DeserialiseFromSession()
        {
            // deserialize from session data (if possible)
            var commandHistroyJson = _session.GetString(typeof(CommandHistory).FullName);

            this._commands = string.IsNullOrWhiteSpace(commandHistroyJson) == false
                                // fill from deserialized regular list of strings
                                ? new FixedLengthList<string>(_maxCommandsCount, System.Text.Json.JsonSerializer.Deserialize<List<string>>(commandHistroyJson))
                                // create empty
                                : new FixedLengthList<string>(_maxCommandsCount);
        }

        public void Push(IEnumerable<string> lines)
        {
            this._commands.Add(lines);

            SerialiseToSession();
        }
    }
}
