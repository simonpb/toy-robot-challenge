﻿namespace MVCToyRobotChallenge.Models
{
    public class CLIModel
    {
        public TabletopModel TabletopModel { get; set; }
        public ConsoleCLIModel ConsoleModel { get; set; }
    }
}
