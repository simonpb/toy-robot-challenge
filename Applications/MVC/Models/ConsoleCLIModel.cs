﻿using System.Collections.Generic;

namespace MVCToyRobotChallenge.Models
{
    public class ConsoleCLIModel
    {
        public string CommandPrompt { get; set; } = "> ";
        public string CommandBuffer { get; set; }
        public IEnumerable<string> Commands { get; set; }        
    }
}
