﻿namespace MVCToyRobotChallenge.Models
{
    public class WASDModel
    {
        public TabletopModel TabletopModel { get; set; }
        public ConsoleWASDModel ConsoleModel { get; set; }
    }
}
