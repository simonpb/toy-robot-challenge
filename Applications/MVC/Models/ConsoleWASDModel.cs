﻿using ToyRobotChallenge.Library.AppSupport.Geometry;

namespace MVCToyRobotChallenge.Models
{
    public class ConsoleWASDModel
    {
        public bool IsRobotPlaced => this.RobotModel?.Vector?.IsRobotPlaced() ?? false;
        public bool IsPlaceDisabled => this.IsRobotPlaced == false;

        public ToyRobotModel RobotModel { get; set; }
        public string Direction { get; set; }
    }
}
