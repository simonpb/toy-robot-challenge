﻿namespace MVCToyRobotChallenge.Models
{
    public class TabletopModel
    {
        public int Rows { get; set; }
        public int Columns { get; set; }
        public ToyRobotModel RobotModel { get; set; }
    }
}
