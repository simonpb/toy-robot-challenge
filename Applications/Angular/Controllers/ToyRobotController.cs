﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Models;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using ToyRobotChallenge.Library.Geometry;
using static ToyRobotChallenge.Library.Simulator;
using ToyRobotChallenge.Library.AppSupport.UserCommands;

namespace NgToyRobotChallenge.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public partial class ToyRobotController
        : ControllerBase
    {
        protected readonly int _tableWidth = 5;
        protected readonly int _tableHeight = 5;
        protected readonly IMediator _mediatR;
        private readonly ILogger<ToyRobotController> _logger;

        public ToyRobotController(IMediator mediatR, ILogger<ToyRobotController> logger)
        {
            this._mediatR = mediatR;
            this._logger = logger;
        }
    }

    /// <summary>
    /// Endpoints
    /// </summary>
    public partial class ToyRobotController
    {
        [HttpPost("start")]
        public async Task<Models.ToyRobotModel> start()
        {
            // execute
            var robot = CreateToyRobotModel(await this._mediatR.Send(new Start((ulong)_tableHeight, (ulong)_tableWidth)));

            // return model
            return robot;
        }

        [HttpPost("turnleft")]
        public async Task<Models.ToyRobotModel> TurnLeft()
        {
            // execute
            var robot = await this._mediatR.Send(new Execute(new LeftTurnCommand()));

            // return model
            return CreateToyRobotModel(robot);
        }

        [HttpPost("turnright")]
        public async Task<Models.ToyRobotModel> TurnRight()
        {
            // execute
            var robot = await this._mediatR.Send(new Execute(new RightTurnCommand()));

            // return model
            return CreateToyRobotModel(robot);
        }

        [HttpPost("move")]
        public async Task<Models.ToyRobotModel> Move()
        {
            // execute
            var robot = await this._mediatR.Send(new Execute(new MoveCommand()));

            // return model
            return CreateToyRobotModel(robot);
        }

        [HttpPost("reset")]
        public async Task<Models.ToyRobotModel> Reset()
        {
            // execute
            var robot = await this._mediatR.Send(new Reset((ulong)_tableHeight, (ulong)_tableWidth));

            // return model
            return CreateToyRobotModel(robot);
        }

        [HttpPost("place")]
        [Consumes("application/json")]
        public async Task<Models.ToyRobotModel> Place([FromBody] Models.ToyRobotModel model)
        {
            var robot = await this._mediatR.Send(new Execute(new PlaceCommand(model.Vector.ToULong())));

            // return model
            return CreateToyRobotModel(robot);
        }

        [HttpPost("execute")]
        [Consumes("multipart/form-data")]
        public async Task<Models.ToyRobotModel> Execute([FromForm] string command)
        {
            var commandList = new CommandList(command ?? string.Empty, true);
            var robot = await this._mediatR.Send(new Execute(commandList));

            // return model
            return CreateToyRobotModel(robot);
        }
    }

    /// <summary>
    /// Support
    /// </summary>
    public partial class ToyRobotController
    {
        private Models.ToyRobotModel CreateToyRobotModel(ToyRobotModel toyRobotModel)
        {
            return new Models.ToyRobotModel(toyRobotModel);
        }
    }
}
