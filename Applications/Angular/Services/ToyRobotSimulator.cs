﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;
using System.Threading.Tasks;
using ToyRobotChallenge.Library;
using ToyRobotChallenge.Library.AppSupport.Application.Implementations;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using ToyRobotChallenge.Library.AppSupport.Models;
using ToyRobotChallenge.Library.Geometry;
using static ToyRobotChallenge.Library.Simulator;

namespace NgToyRobotChallenge.Services
{
    public class ToyRobotSimulator
        : StockToyRobotSimulator
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;

        public ToyRobotSimulator(IHttpContextAccessor httpContextAccessor)
        {
            this._httpContextAccessor = httpContextAccessor;
            this._session = _httpContextAccessor.HttpContext.Session;

            DeserialiseFromSession();
        }

        public override async Task StartAsync(ulong rows, ulong columns)
        {
            await base.StartAsync(rows, columns);

            SerialiseToSession();
        }

        public override async Task<ToyRobotModel> ExecuteAsync(IEnumerable<IRobotCommand> commands)
        {
            var robotModel = await base.ExecuteAsync(commands);

            SerialiseToSession();

            return robotModel;
        }

        private void SerialiseToSession()
        {
            // serialise to session
            if ((this.Simulator is null) == false)
            {
                var board = this.Simulator.TheRobot.Board;
                var robot = this.Robot;

                var tableTopJson = System.Text.Json.JsonSerializer.Serialize(new Models.TabletopModel() { Columns = (int)board.Width, Rows = (int)board.Height, RobotModel = null });
                var robotJson = System.Text.Json.JsonSerializer.Serialize(new Models.ToyRobotModel(robot));

                this._session.SetString(typeof(Models.TabletopModel).FullName, tableTopJson);
                this._session.SetString(typeof(Models.ToyRobotModel).FullName, robotJson);
            }
            else
            {
                this._session.Remove(typeof(Models.TabletopModel).FullName);
                this._session.Remove(typeof(Models.ToyRobotModel).FullName);
            }
        }

        private void DeserialiseFromSession()
        { 
            var robotJson = _session.GetString(typeof(Models.ToyRobotModel).FullName);
            var tableTopJson = _session.GetString(typeof(Models.TabletopModel).FullName);

            if (true
                && string.IsNullOrWhiteSpace(robotJson) == false
                && string.IsNullOrWhiteSpace(tableTopJson) == false)
            {
                var tableTopModel = System.Text.Json.JsonSerializer.Deserialize<Models.TabletopModel>(tableTopJson);
                var robotModel = System.Text.Json.JsonSerializer.Deserialize<Models.ToyRobotModel>(robotJson);

                var board = new Board((ulong)tableTopModel.Rows, (ulong)tableTopModel.Columns);
                var robot = new ToyRobot(board) { Vector = robotModel.Vector.IsRobotPlaced() ? robotModel.Vector.ToULong() : null };
                var simulator = new Simulator(robot);

                this.Simulator = simulator;
                this.Robot = new ToyRobotModel() { Vector = robot.Vector, State = robotModel.State };
            }
        }
    }
}
