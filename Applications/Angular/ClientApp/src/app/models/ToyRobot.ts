
export enum Direction {
  North,
  East,
  South,
  West
}

export enum State {
  None,
  Standing,
  Waving,
  Jumping
}

export class Position {
  public x: Number;
  public y: Number;

  constructor(x: Number = -1, y: Number = -1) {
    this.x = x;
    this.y = y;
  }
}

export class Vector {
  public position: Position;
  public dir: Direction;

  constructor(position: Position = new Position(), dir: Direction = Direction.North) {
    this.dir = dir;
    this.position = position;
  }
}

/*
 * Toy robot model
 */
export class ToyRobot {
  public vector: Vector;
  public state: State;

  constructor(vector:Vector = new Vector(), state:State = State.None) {
    this.vector = vector;
    this.state = state;
  }
}
