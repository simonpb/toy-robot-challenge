import { Component } from '@angular/core';
import { SimulatorService } from '../../services/Simulator';
import { ToyRobot } from '../../models/ToyRobot';
import { CommandHistoryService } from './../../services/CommandHistory'

@Component({
  selector: 'app-simulator-cli',
  templateUrl: './simulator-cli.component.html'
})
export class SimulatorCliComponent {

  // private fields
  private simulator: SimulatorService;
  private commandHistory: CommandHistoryService;
  private robot: ToyRobot;

  // ctor
  constructor(simulator: SimulatorService, commandHistory: CommandHistoryService) {

    this.simulator = simulator;
    this.commandHistory = commandHistory;

    // start the simulation (if it's not already started)
    this
      .simulator
      .start()
      .subscribe(
        robot => {
          this.robot = robot;
        },
        error => {
          console.error(error);
        });
  }

  // process command event
  public onCommand(cmd: String): void {

    this.commandHistory.addCommandsToHistory([cmd]);

    if (cmd.toLowerCase() === 'help' || cmd.toLowerCase() === 'h') {

      this.executeDisplayHelp();
    }
    else if (cmd.toLowerCase() === 'report') {

      this.executeDisplayReport();
    }
    else {
      // execute command
      this
        .simulator
        .execute(cmd)
        .subscribe(
          robot => {
            this.robot = robot;
          },
          error => {
            console.error(error);
          });
    }
  }

  // display help in command history
  private executeDisplayHelp(): void {

    let lines = [

      "Use the following commands to ready & control your robot (commands are case insensitive!):",
      "PLACE [x],[y], [FACING]\" - Place the robot in the required x,y position in the required direction.",
      "x,y are the grid coordinates 0 -> 5 for width & 0 -> 5 for height.",
      "    Facing is one of NORTH, EAST, SOUTH, WEST",
      "You must place your robot first, any other interactive commands will be ignored until the robot has been placed.",
      "You may place your robot as many tmes as you like.",
      "MOVE - Move the robot by 1 grid square in the direction it is facing.",
      "LEFT - Rotate the robot 90 degrees left in the current grid position.",
      "RIGHT - Rotate the robot 90 degrees right in the current grid position.",
      "REPORT - Display the robots current position & direction.",
      "WAVE - Give a little wave :-)",
      "JUMP - Give a little jump :-P"
    ];

    this.commandHistory.addCommandsToHistory(lines);
  }

  // display report in command history
  private executeDisplayReport(): void {

    if (this.robot !== null && this.robot.vector !== null && this.robot.state.toString() !== 'None') {
      this.commandHistory.addCommandsToHistory(['Position: (' + this.robot.vector.position.x + ', ' + this.robot.vector.position.y + ') Direction: ' + this.robot.vector.dir]);
    }
    else {
      this.commandHistory.addCommandsToHistory([ 'Robot MUST be placed first!' ]);
    }
  }
}

