import { Component } from '@angular/core';
import { SimulatorService } from '../../services/Simulator';
import { ToyRobot } from '../../models/ToyRobot';

@Component({
  selector: 'app-simulator-wasd',
  templateUrl: './simulator-wasd.component.html'
})
export class SimulatorWasdComponent {

  // private fields
  private simulator: SimulatorService;
  private robot: ToyRobot;

  // ctor
  constructor(simulator: SimulatorService) {

    this.simulator = simulator;

    // start the simulation (if it's not already started)
    this
      .simulator
      .start()
      .subscribe(
        robot => {
          this.robot = robot;
        },
        error => {
          console.error(error);
        });
  }

  public onCommand(cmd: String): void {

    // execute command
    this
      .simulator
      .execute(cmd)
      .subscribe(
        robot => {
          this.robot = robot;
        },
        error => {
          console.error(error);
        });
  }
}

