import { Component, EventEmitter, Output, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { CommandHistoryService } from './../../services/CommandHistory'

@Component({
  selector: 'app-console-cli',
  templateUrl: './console-cli.component.html',
  styleUrls: ['./console-cli.component.css']
})
export class ConsoleCliComponent implements OnInit, OnDestroy {

  // private fields
  private commandsSubscription: Subscription;

  // public fields
  public commands: Array<String>;
  public commandHistory: CommandHistoryService;

  // I/O
  @Output() onCommand = new EventEmitter<String>();

  // ctor
  constructor(commandHistory: CommandHistoryService) {

    this.commands = commandHistory.getCommandHistory();
    this.commandHistory = commandHistory;
  }

  // event handlers...
  public onEnter(event: any): void {

    // get the input data
    let command: String = event.target.value;

    // cleanup
    command.trim();

    // add to command history (if not empty)
    if (command.length > 0) {
      // raise command event with value
      this.onCommand.emit(command);
    }

    // clear input
    event.target.value = '';
  }

  public onCommandsUpdated(commands: Array<String>): void {
    this.commands = commands;
  }

  public ngOnInit(): void {
    // subscribe to command history
    this.commandsSubscription = this.commandHistory.commands$.subscribe((commands: Array<String>) => { this.onCommandsUpdated(commands) });
  }

  public ngOnDestroy(): void {
    // unsubscript from command history
    this.commandsSubscription.unsubscribe();
  }
}

