import { Component, OnInit, Input } from '@angular/core';
import { ToyRobot, Position } from '../../models/ToyRobot';

@Component({
  selector: 'app-robot',
  templateUrl: './robot.component.html',
  styleUrls: ['./robot.component.css']
})
export class RobotComponent implements OnInit {

  // private fields
  private _robot: ToyRobot = null;

  // getter/setting for easy debugging...
  @Input()
  get robot(): ToyRobot {
    return this._robot;
  }
  set robot(robot: ToyRobot) {
    // don't allow to be set to undefined
    this._robot = robot === undefined ? null : robot;
  }

  constructor() {

    this.robot = new ToyRobot();
  }

  ngOnInit() {
  }
}

