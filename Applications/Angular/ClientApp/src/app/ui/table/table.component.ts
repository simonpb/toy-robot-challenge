import { Component, OnInit, Input } from '@angular/core';
import { ToyRobot } from '../../models/ToyRobot';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  // input fields
  @Input() rowCount: Number = 5;
  @Input() columnCount: Number = 5;

  // getter/setting for easy debugging...
  @Input()
  get robot(): ToyRobot {
    return this._robot;
  }
  set robot(robot: ToyRobot) {
    // don't allow to be set to undefined
    this._robot = robot === undefined ? null : robot;
  }

  // private fields
  private _robot: ToyRobot = null;

  // public fields
  public rows: Array<Number>;
  public columns: Array<Number>;

  // ctor
  constructor() {
  }

  public ngOnInit(): void {

    // fill rows and column arrays...
    this.rows = Array(this.rowCount).fill(0).map((x, i) => i);
    this.columns = Array(this.columnCount).fill(0).map((x, i) => i);
    this.robot = new ToyRobot();
  }

  // Convenience function to test is robot is at specific position
  public isRobotAtPosition(x: Number, y: Number): Boolean {

    if (this.robot === null) {
      return false;
    }

    if (true
      && this.robot.vector.position.x === x
      && this.robot.vector.position.y === y) {

      // robot is in specified position
      return true;
    }

    return false;
  }
}

