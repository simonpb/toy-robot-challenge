import { Component, EventEmitter, Input, Output, OnInit, OnDestroy } from '@angular/core';
import { ToyRobot } from '../../models/ToyRobot';

@Component({
  selector: 'app-console-wasd',
  templateUrl: './console-wasd.component.html',
  styleUrls: ['./console-wasd.component.css']
})
export class ConsoleWasdComponent implements OnInit, OnDestroy {

  // getter/setting for easy debugging...
  @Input()
  get robot(): ToyRobot {
    return this._robot;
  }
  set robot(robot: ToyRobot) {
    // don't allow to be set to undefined
    this._robot = robot === undefined ? null : robot;
  }

  // private fields
  private _robot: ToyRobot = null;

  // public fields
  public direction: String = '';
  public x: Number = -1;
  public y: Number = -1;

  // I/O
  @Output() onCommand = new EventEmitter<String>();

  // ctor
  constructor() {
  }

  // Convenience function to test if robot is placed
  public isRobotPlaced(): Boolean {

    if (false
        // check for null
        || this.robot === null
        || this.robot.vector === null
        || this.robot.vector.position === null
        || this.robot.state === null
        || this.robot.state.toString().trim() === '') {

      return false;
    }

    if (true
      && this.robot.state.toString().trim() !== 'None'
      && this.robot.vector.position.x >= 0
      && this.robot.vector.position.y >= 0) {

      // robot is in specified position
      return true;
    }

    return false;
  }

  public onPlace(): void {
    this.onCommand.emit('PLACE ' + this.x + ',' + this.y + ',' + this.direction);
  }

  public onTurnLeft(): void {
    this.onCommand.emit('LEFT');
  }

  public onTurnRight(): void {
    this.onCommand.emit('RIGHT');
  }

  public onMove(): void {
    this.onCommand.emit('MOVE');
  }

  public onReset(): void {
    this.onCommand.emit('QUIT');
  }

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }
}

