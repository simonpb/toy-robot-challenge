import { Inject, Injectable                           } from '@angular/core';
import { HttpClient, HttpHeaders                      } from '@angular/common/http';
import { ToyRobot, Vector, Position, Direction, State } from '../models/ToyRobot';
import { Observable, throwError                       } from 'rxjs';
import { retry, catchError                            } from 'rxjs/operators';

/*
 * Simulator service
 * Implemented as per https://www.remotestack.io/angular-httpclient-service-example-tutorial/
 */
@Injectable()
export class SimulatorService {

  // private fields
  private httpClient: HttpClient;
  private baseUrl: String;

  // ctor
  constructor(httpClient: HttpClient, @Inject('BASE_URL') baseUrl: string) {

    this.httpClient = httpClient;
    this.baseUrl = baseUrl;
  }

  // Reset the simulation (if its already started)
  public reset(): Observable<ToyRobot> {

    // POST reset
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/reset', null)
            .pipe(retry(1), catchError(this.httpError));
  }

  // Start the simulation (if its NOT already started)
  public start(): Observable<ToyRobot> {

    // POST start 
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/start', null)
            .pipe(retry(1), catchError(this.httpError));
  }

  // place the robot on the board
  public place(x: Number, y: Number, dir: Direction): Observable<ToyRobot> {

    // POST place

    // create robot model...
    let toyRobot = new ToyRobot(new Vector(new Position(x, y), dir), State.Standing);

    // create header options...
    let httpOptions = { headers: new HttpHeaders({ 'Content-Type': 'application/json' }) };

    // POST place
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/place', toyRobot, httpOptions)
            .pipe(retry(1), catchError(this.httpError));
  }

  // Move the robot
  public move(): Observable<ToyRobot> {

    // POST move 
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/move', null)
            .pipe(retry(1), catchError(this.httpError));
  }

  // Turn the robot left
  public turnLeft(): Observable<ToyRobot> {

    // POST turnLeft 
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/turnLeft', null)
            .pipe(retry(1), catchError(this.httpError));
  }

  //Turn the robot right
  public turnRight(): Observable<ToyRobot> {

    // POST turnLeft 
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/turnRight', null)
            .pipe(retry(1), catchError(this.httpError));
  }

  // Execute a command
  public execute(command: String): Observable<ToyRobot> {

    // https://www.positronx.io/how-to-use-angular-8-httpclient-to-post-formdata/

    // fill form data
    let data = new FormData();

    data.append('command', command.toString());

    // POST command
    return this
            .httpClient
            .post<ToyRobot>(this.baseUrl + 'toyrobot/execute', data)
            .pipe(retry(1), catchError(this.httpError));
  }

  // Generic http error handling
  private httpError(error) {

    let msg = (error.error instanceof ErrorEvent)
                // client side error
                ? error.error.message
                // server side error
                : `Error Code: ${error.status}\nMessage: ${error.message}`;

    console.log(msg);

    return throwError(msg);
  }
}
