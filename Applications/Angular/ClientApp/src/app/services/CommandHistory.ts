import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

/*
 * Command history service
 */
@Injectable()
export class CommandHistoryService {

  // private fields
  private maxCommands: Number;
  private commands: Array<String>;

  // subjects to observe
  private commandsSubject: Subject<Array<String>>;

  // public observables
  public commands$: Observable<Array<String>>;

  // ctor
  constructor() {

    // initialise commands
    this.maxCommands = 11;
    this.commands = new Array();

    // subject to observe
    this.commandsSubject = new Subject<Array<String>>();

    // the subject observer
    this.commands$ = this.commandsSubject.asObservable();
  }

  // return current command history
  public getCommandHistory(): Array<String> {
    return this.commands;
  }

  // add array of commands to history
  public addCommandsToHistory(commands: String[]): Array<String> {

    // push commands
    this.commands.push(...commands);

    // truncate command history
    while (this.commands.length > this.maxCommands) {
      this.commands.shift();
    }

    // cast
    this.commandsSubject.next(this.commands);

    return this.commands;
  }
}
