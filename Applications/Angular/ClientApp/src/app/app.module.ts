// modules
import { BrowserModule    } from '@angular/platform-browser';
import { NgModule         } from '@angular/core';
import { FormsModule      } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { RouterModule     } from '@angular/router';

// root components
import { AppComponent     }       from './app.component';
import { NavMenuComponent }       from './ui/nav-menu/nav-menu.component';

// TRC components
import { SimulatorCliComponent  } from './ui/simulator-cli/simulator-cli.component';
import { SimulatorWasdComponent } from './ui/simulator-wasd/simulator-wasd.component';
import { ConsoleCliComponent    } from './ui/console-cli/console-cli.component';
import { ConsoleWasdComponent   } from './ui/console-wasd/console-wasd.component';
import { TableComponent         } from './ui/table/table.component';
import { RobotComponent         } from './ui/robot/robot.component';

// services
import { SimulatorService       } from './services/Simulator';
import { CommandHistoryService  } from './services/CommandHistory'


@NgModule({
  declarations: [
    AppComponent,
    NavMenuComponent,
    SimulatorCliComponent,
    SimulatorWasdComponent,
    ConsoleCliComponent,
    ConsoleWasdComponent,
    TableComponent,
    RobotComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'ng-cli-universal' }),
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot([
      { path: '', component: SimulatorCliComponent, pathMatch: 'full' },
      { path: 'simulator-wasd', component: SimulatorWasdComponent },
    ])
  ],
  providers: [
    SimulatorService,
    CommandHistoryService
  ],
  bootstrap: [AppComponent]
})

export class AppModule { }
