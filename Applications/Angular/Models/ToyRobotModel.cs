﻿using ToyRobotChallenge.Library.Geometry;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using static ToyRobotChallenge.Library.AppSupport.Models.ToyRobotModel;

namespace NgToyRobotChallenge.Models
{
    public class ToyRobotModel
    {
        public Vector2d<int> Vector { get; set; }
        public RobotState State { get; set; }

        public ToyRobotModel()
            : this(null)
        {
        }

        public ToyRobotModel(ToyRobotChallenge.Library.AppSupport.Models.ToyRobotModel robot)
        {
            this.Vector = robot?.Vector is null
                            ? new Vector2d<int>(new Point2d<int>(-1, -1), Direction.North)
                            : robot.Vector.ToInt();
            this.State = robot is null
                            ? RobotState.None
                            : robot.State;
        }
    }
}
