﻿using ToyRobotChallenge.Library.Geometry;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using static ToyRobotChallenge.Library.AppSupport.Models.ToyRobotModel;
using System.Text.Json.Serialization;

namespace VueToyRobotChallenge.Models
{
    public class ToyRobotModel
    {
        public int X { get; set; }

        public int Y { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public Direction Direction { get; set; }

        [JsonConverter(typeof(JsonStringEnumConverter))]
        public RobotState State { get; set; }

        public ToyRobotModel()
            : this(null)
        {
        }

        public ToyRobotModel(ToyRobotChallenge.Library.AppSupport.Models.ToyRobotModel robot)
        {
            if(robot is null)
            {
                this.X = -1;
                this.Y = -1;
                this.Direction = Direction.North;
                this.State = RobotState.None;
            }
            else
            {
                var v = robot.Vector?.ToInt();

                this.X = v?.Position?.X ?? -1;
                this.Y = v?.Position?.Y ?? -1;
                this.Direction = v?.Dir ?? Direction.North;
                this.State = robot.State;
            }
        }
    }
}
