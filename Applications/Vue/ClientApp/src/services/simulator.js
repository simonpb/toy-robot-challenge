﻿import { ToyRobot } from "@/models/toyrobot.js";
import axios from 'axios'

var simulator = (function () {

    return {
        reset: function () {
            return axios.post('/toyrobot/reset');
        },
        start: function () {
            return axios.post('/toyrobot/start');
        },
        place: function (x, y, direction) {
            let r = new ToyRobot();

            r.x = x;
            r.y = y;
            r.state = 'Standing';
            r.direction = direction;

            let json = JSON.stringify(r);
            let url = '/toyrobot/place';

            return axios({
                method: 'post',
                url: url,
                data: json,
                headers: { 'Content-Type': 'application/json' },
            });
        },
        move: function () {
            return axios.post('/toyrobot/move');
        },
        turnLeft: function () {
            return axios.post('/toyrobot/turnleft');
        },
        turnRight: function () {
            return axios.post('/toyrobot/turnright');
        },
        execute: function (command) {
            let data = new FormData();
            let url = '/toyrobot/execute';

            data.append('command', command);

            return axios({
                method: 'post',
                url: url,
                data: data,
                headers: { 'Content-Type': 'multipart/form-data' },
            });
        }
    }
})();

export { simulator }