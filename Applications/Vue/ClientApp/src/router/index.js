﻿import { createWebHistory, createRouter } from "vue-router";
import CLI from "@/components/SimulatorCLI.vue";
import WASD from "@/components/SimulatorWASD.vue";

const routes = [
    {
        path: "/",
        name: "Home",
        component: CLI,
    },
    {
        path: "/CLI",
        name: "CLI",
        component: CLI,
    },
    {
        path: "/WASD",
        name: "WASD",
        component: WASD,
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;