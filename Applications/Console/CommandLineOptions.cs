﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ConsoleToyRobotChallenge
{
    /// <summary>
    /// class to encapsulate command line options
    /// </summary>
    public class CommandLineOptions
    {
        /// <summary>
        /// Supported command line runtime options
        /// </summary>
        private static string cmdLineOptionHelp = "-help";
        private static string cmdLineOptionInteractive = "-interactive";
        private static string cmdLineOptionCmsCaseInsensitive = "-nocase";

        /// <summary>
        /// original command line options
        /// </summary>
        private string[] _args;

        /// <summary>
        /// List of command line options that are could be input files (ie. they are NOT runtime recodnised as runtime options).  
        /// No valiation on the input file existance or content happens though then this list is built
        /// </summary>
        public IList<string> InputFiles { get; } = new List<string>();

        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="args">command line arguments</param>
        public CommandLineOptions(string[] args)
        {
            Debug.Assert(args != null);

            // retain args
            this._args = args;

            // build list of input files
            foreach (var arg in args)
            {
                if (!CommandLineOptions.IsCommandLineOption(arg))
                {
                    InputFiles.Add(arg);
                }
            }
        }

        private static bool IsCommandLineOption(string str)
        {
            return  false
                    || (string.Compare(str, cmdLineOptionHelp) == 0
                    || string.Compare(str, cmdLineOptionInteractive) == 0
                    || string.Compare(str, cmdLineOptionCmsCaseInsensitive) == 0);
        }

        public bool DoShowHelp()
        {
            var value = _args.FirstOrDefault(
                (arg) =>
                {
                    return arg == cmdLineOptionHelp;
                });

            return value != null;
        }

        public bool DoRunInteractiveSimulator()
        {
            var value = _args.FirstOrDefault(
                (arg) =>
                {
                    return arg == cmdLineOptionInteractive;
                });

            return value != null;
        }

        public bool DoAllowCaseInsensitiveRobotCmds()
        {
            var value = _args.FirstOrDefault(
                (arg) =>
                {
                    return arg == cmdLineOptionCmsCaseInsensitive;
                });

            return value != null;
        }
    }
}
