﻿using System;
using System.Diagnostics;
using ToyRobotChallenge.Library;
using ToyRobotChallenge.Library.CommandLists;

namespace ConsoleToyRobotChallenge.SimulatorApp
{
    /// <summary>
    /// An interactive simulator, let the user enter robot commands
    /// </summary>
    public class InteractiveSimulator
        : SimulatorApp
        , Simulator.IStringResponse
    {
        private readonly IRobot _robot;

        public InteractiveSimulator(IRobot robot, bool ignoreCmdCase)
            : base(ignoreCmdCase)
        {
            Debug.Assert(robot != null);

            this._robot = robot;
        }

        public override void Run()
        {
            // create simulator
            var simulator = new Simulator(_robot);

            Debug.Assert(simulator != null);

            Console.WriteLine("");
            Console.WriteLine("Now in interactive mode.");
            Console.WriteLine($"Board size is {_robot?.Board?.Width ?? 0}x{_robot?.Board?.Height ?? 0}");
            Console.WriteLine($"Robot commands are { (_ignoreCmdCase ? "case insensitive" : "case sensitive") }");
            Console.WriteLine("Use the command q or exit to quit.");
            Console.WriteLine("");
            Console.WriteLine("Use the following commands to ready & control your robot (commands are case sensitive!):");
            Console.WriteLine("\"PLACE [x],[y], [FACING]\" - Place the robot in the required x,y position in the required direction.");
            Console.WriteLine($"  x,y are the grid coordinates 0 -> {(_robot?.Board?.Width ?? 0) - 1 } for width & 0->{(_robot?.Board?.Height ?? 0) - 1} for height.");
            Console.WriteLine("  Facing is one of NORTH, EAST, SOUTH, WEST");
            Console.WriteLine("");
            Console.WriteLine("You must place your robot first, any other interactive commands will be ignored until the robot has been placed.");
            Console.WriteLine("You may place your robot as many tmes as you like.");
            Console.WriteLine("");
            Console.WriteLine("MOVE - Move the robot by 1 grid square in the direction it is facing.");
            Console.WriteLine("LEFT - Rotate the robot 90 degrees left in the current grid position.");
            Console.WriteLine("RIGHT - Rotate the robot 90 degrees right in the current grid position.");
            Console.WriteLine("REPORT - Display the robots current position & direction.");


            while (true)
            {
                var command = Console.ReadLine();

                if(IsUserWantsToQuit(command))
                {
                    break;
                }

                // assume a command string was supplied.
                simulator.Execute(new CommandList(command, _ignoreCmdCase), this);
            }
        }

        public void Write(string response)
        {
            Console.WriteLine(response);
        }

        private bool IsUserWantsToQuit(string command)
        {
            // user quits
            if (false
                || string.Compare(command, "q", true) == 0
                || string.Compare(command, "quit", true) == 0
                || string.Compare(command, "exit", true) == 0)
            {
                return true;
            }

            return false;
        }
    }
}
