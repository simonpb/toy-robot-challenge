﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using ToyRobotChallenge.Library;
using ToyRobotChallenge.Library.CommandLists;

namespace ConsoleToyRobotChallenge.SimulatorApp
{
    /// <summary>
    /// A command file processing simulator
    /// </summary>
    public class CommandFileProcessingSimulator 
        : SimulatorApp
    {
        private class Response
            : Simulator.IStringResponse
        {
            public void Write(string response)
            {
                Console.WriteLine(response);
            }
        }

        private readonly Simulator.IStringResponse _response;
        private readonly IEnumerable<string> _inputFiles;
        private readonly IRobot _robot;

        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="ignoreCmdCase">ignore case?</param>
        /// <param name="inputFiles">list of robot command input files to process</param>
        public CommandFileProcessingSimulator(  IRobot robot,
                                                bool ignoreCmdCase, 
                                                IEnumerable<string> inputFiles)
            : base(ignoreCmdCase)
        {
            Debug.Assert(robot != null);
            Debug.Assert(inputFiles != null);

            this._inputFiles = inputFiles;
            this._robot = robot;
            this._response = new Response();
        }

        public override void Run()
        {
            // create simulator for robot
            var simulator = new Simulator(_robot);

            Debug.Assert(simulator != null);

            Console.WriteLine("");
            Console.WriteLine("Processing input command files.");
            Console.WriteLine($"Board size is {_robot?.Board?.Width ?? 0}x{_robot?.Board?.Height ?? 0}");
            Console.WriteLine($"Robot commands are { (_ignoreCmdCase ? "case insensitive" : "case sensitive") }");

            foreach (var filename in _inputFiles)
            {
                ExecuteRobotCommandsInFile(filename, simulator);
            }
        }

        private void ExecuteRobotCommandsInFile(string filename, Simulator simulator)
        {
            Debug.Assert(simulator != null);
            Console.WriteLine($"Processing input command file \"{filename}\"");

            // Read the file and process each line
            var file = new System.IO.StreamReader(filename);
            string command;

            while ((command = file.ReadLine()) != null)
            {
                ExecuteRobotCommandInFile(command, simulator);
            }

            file.Close();
        }

        private void ExecuteRobotCommandInFile(string command, Simulator simulator)
        {
            if (command.Length <= 0)
                return;

            if (command[0] == '#')
                // comment line, ignore it
                return;

            if (command.Length >= 5
                && command.Substring(0, 5) == "echo ")
            {
                Console.WriteLine(command.Substring(5));
                return;
            }

            // assume a command string was supplied.
            simulator.Execute(new CommandList(command, _ignoreCmdCase), this._response);
        }


    }
}
