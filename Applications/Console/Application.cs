﻿using System;
using System.Linq;
using ToyRobotChallenge.Library;

namespace ConsoleToyRobotChallenge
{
    /// <summary>
    /// Class to encapsulate the concept of an application...
    /// </summary>
    class Application
    {
        private CommandLineOptions _cmdLineOptions;

        /// <summary>
        /// default ctor
        /// </summary>
        /// <param name="args">command line arguments</param>
        public Application(string[] args)
        {
            _cmdLineOptions = new CommandLineOptions(args);
        }

        /// <summary>
        /// Go go go!
        /// </summary>
        public void Run()
        {
            try
            {
                if (_cmdLineOptions.DoShowHelp())
                {
                    ShowHelp();
                }

                // process input files
                RunCommandFileProcessingSimulator();

                // run interactive toy robot simulator
                if (_cmdLineOptions.DoRunInteractiveSimulator())
                {
                    RunInteractiveToyRobotSimulator();
                }
                else
                {
                    Console.ReadKey();
                }
            }
            catch (Exception e)
            {
                Console.WriteLine($"Exception captured in top level entry method {nameof(Application)} of the \"Toy Robot Challange application\".  Exception message: {e.Message}");
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Show application help
        /// </summary>
        private void ShowHelp()
        {
            Console.WriteLine("");
            Console.WriteLine("ConsoleToyRobotChallenge.exe [-help] [-interactive] <input file 1> <input file 2> ... <input file n>");
            Console.WriteLine("-help\t\tDisplayes this help.");
            Console.WriteLine("-interactive\tEnters interactive mode, use commands to control your robot.");

            Console.WriteLine("-nocase\t\tBy default robot commands are case sensitive, use this option to allow case insensitive commands in interactive mode");
            Console.WriteLine("\t\tand when processing commands from an input file.");

            Console.WriteLine("<input file n>\t0 or more intput files from which to extract and execute robot commands.");
        }

        /// <summary>
        /// Run the command file processing simulator
        /// </summary>
        /// <returns></returns>
        private void RunCommandFileProcessingSimulator()
        {
            if (_cmdLineOptions.InputFiles.Count() > 0)
            {
                // create and run the batch processing simulator
                var simulatorApp = new SimulatorApp.CommandFileProcessingSimulator( new ToyRobot(new Board(5, 5)),
                                                                                    _cmdLineOptions.DoAllowCaseInsensitiveRobotCmds(),
                                                                                    _cmdLineOptions.InputFiles);
                simulatorApp.Run();
            }
        }

        /// <summary>
        /// Run the interactive simulator
        /// </summary>
        /// <returns></returns>
        private void RunInteractiveToyRobotSimulator()
        {
            // create and run the interactive simulator
            var simulatorApp = new SimulatorApp.InteractiveSimulator(new ToyRobot(new Board(5, 5)), 
                                                                     _cmdLineOptions.DoAllowCaseInsensitiveRobotCmds());
            simulatorApp.Run();
        }    
    }
}
