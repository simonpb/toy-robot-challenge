﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using ToyRobotChallenge.Library.CommandLists;

namespace UnitTests
{
    [TestClass]
    public class ConsoleCommandListTest
    {
        [TestMethod]
        public void WhenParsingCoordinatesThatAreToBigIgnoreTheCommand()
        {
            var cmdString = "PLACE 18446744073709551616,18446744073709551616,EAST";
            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(0, commandList.Count);
        }

        [TestMethod]
        public void WhenParsingCoordinatesThatAreToNegativeIgnoreTheCommand()
        {
            var cmdString = "PLACE -4,-1,WEST";
            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(0, commandList.Count);
        }

        [TestMethod]
        public void WhenParsingMultipleCommandsOnASingleLineCreateMultipleCommands()
        {
            var cmdString = "PLACE 4,1,WEST MOVE MOVE REPORT PLACE 4,1,WEST";
            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(5, commandList.Count);
        }

        [TestMethod]
        public void WhenParsingMultipleCommandsOnAMultipleLineCreateMultipleCommands()
        {
            var cmdString = "PLACE 4,1,WEST\n" +
                            "MOVE \n" + 
                            "MOVE \n" +
                            "REPORT \n" +
                            "PLACE 4,1,WEST\n";

            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(5, commandList.Count);
        }

        [TestMethod]
        public void WhenParsingMultipleCommandsOnASingleLineIgnoreUnknownCommandsInbetweenAndCreateMultipleCommands()
        {
            var cmdString = "PLACE PLACE 4,1,WEST MOVE MOVE REPORT PLACE 4,1,WEST";
            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(5, commandList.Count);
        }

        [TestMethod]
        public void WhenParsingMultipleCommandsOnMultipleLinesIgnoreUnknownCommandsInbetweenAndCreateMultipleCommands()
        {
            var cmdString = "PLACE 4,1,WEST asdfsdaffg\n" +
                            "MOVE ddfadf\n" +
                            " dadfafddeef MOVE \n" +
                            "fdadfas REPORT RepORT\n" +
                            "PLACE PLACE PLACE 4,1,WEST asdfasdf\n";

            var commandList = new CommandList(cmdString, false);

            Assert.IsNotNull(commandList);
            Assert.AreEqual(5, commandList.Count);
        }
    }
}
