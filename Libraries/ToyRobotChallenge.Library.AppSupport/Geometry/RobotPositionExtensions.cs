﻿using ToyRobotChallenge.Library.Geometry;

namespace ToyRobotChallenge.Library.AppSupport.Geometry
{
    public static class Vector2dExtensions
    {
        public static bool IsRobotPlaced(this Vector2d<int> v)
        {
            return v != null && v.Position.X >= 0 && v.Position.Y >= 0;
        }

        public static bool IsRobotPlaced(this Vector2d<ulong> v)
        {
            return v != null && v.Position.X >= 0 && v.Position.Y >= 0;
        }

        public static Vector2d<int> ToInt(this Vector2d<ulong> v)
        {
            return (v is null)
                ? null
                : new Vector2d<int>(new Point2d<int>((int)v.Position.X, (int)v.Position.Y), v.Dir);
        }

        public static Vector2d<ulong> ToULong(this Vector2d<int> v)
        {
            return (v is null)
                ? null
                : new Vector2d<ulong>(new Point2d<ulong>((ulong)v.Position.X, (ulong)v.Position.Y), v.Dir);
        }
    }
}
