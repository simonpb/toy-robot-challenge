﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.Interfaces;
using ToyRobotChallenge.Library.AppSupport.Geometry;
using ToyRobotChallenge.Library.AppSupport.Models;

namespace ToyRobotChallenge.Library.AppSupport.Application.Implementations
{
    /// <summary>
    /// Stock implementation of <see cref="IToyRobotSimulator"/>
    /// </summary>
    public class StockToyRobotSimulator
        : IToyRobotSimulator
    {
        public Simulator Simulator { get; protected set; }
        public ToyRobotModel Robot { get; set; }
        public bool HasStarted => (Simulator is null) == false;

        public virtual Task ResetAsync(ulong rows, ulong columns)
        {
            return this.StartAsync(rows, columns);
        }

        public virtual Task StartAsync(ulong rows, ulong columns)
        {
            var board = new Board(rows, columns);
            var robot = new ToyRobot(board);
            var simulator = new Simulator(robot);

            this.Simulator = simulator;
            this.Robot = null;

            return Task.CompletedTask;
        }

        public virtual async Task<ToyRobotModel> ExecuteAsync(IEnumerable<Simulator.IRobotCommand> commands)
        {
            var robotState = this.Robot?.State ?? ToyRobotModel.RobotState.None;

            foreach (var command in commands)
            {
                if(command is ExtraRobotCommands.WaveCommand)
                {                    
                    if (this.Simulator?.TheRobot?.HasAPosition ?? false)
                    {
                        var doWave = (command as ExtraRobotCommands.WaveCommand).WaveToUser;

                        if (doWave.HasValue)
                        {
                            // as per value
                            robotState = (command as ExtraRobotCommands.WaveCommand).WaveToUser == true
                                            ? ToyRobotModel.RobotState.Waving
                                            : ToyRobotModel.RobotState.Standing;
                        }
                        else
                        {
                            // toggle
                            robotState = robotState == ToyRobotModel.RobotState.Waving
                                            ? ToyRobotModel.RobotState.Standing
                                            : ToyRobotModel.RobotState.Waving;
                        }
                    }
                }
                else if(command is ExtraRobotCommands.JumpCommand)
                {
                    if (this.Simulator?.TheRobot?.HasAPosition ?? false)
                    {
                        var doJump = (command as ExtraRobotCommands.JumpCommand).DoJump;

                        if (doJump.HasValue)
                        {
                            // as per value
                            robotState = (command as ExtraRobotCommands.JumpCommand).DoJump == true
                                            ? ToyRobotModel.RobotState.Jumping
                                            : ToyRobotModel.RobotState.Standing;
                        }
                        else
                        {
                            // toggle
                            robotState = robotState == ToyRobotModel.RobotState.Jumping
                                            ? ToyRobotModel.RobotState.Standing
                                            : ToyRobotModel.RobotState.Jumping;
                        }
                    }
                }
                else if(command is ExtraRobotCommands.QuitCommand)
                {
                    if(this.Simulator?.TheRobot?.HasAPosition ?? false)
                    {
                        // reset with existing board dimenstions
                        await ResetAsync(this.Simulator.TheRobot.Board.Width, this.Simulator.TheRobot.Board.Width);

                        // reset robot state
                        robotState = ToyRobotModel.RobotState.None;
                    }
                    else
                    {
                        // nothing to do!
                    }
                }
                else if(false
                        || command is Simulator.LeftTurnCommand 
                        || command is Simulator.MoveCommand 
                        || command is Simulator.PlaceCommand 
                        || command is Simulator.RightTurnCommand)
                {
                    this.Simulator.Execute(command, null);

                    // reset state back to standing if stock commands (other than report and validate) are executed (assuming robot is actually placed)
                    robotState = this.Simulator?.TheRobot?.HasAPosition ?? false
                                    ? ToyRobotModel.RobotState.Standing
                                    : ToyRobotModel.RobotState.None;
                }
                else
                {
                    this.Simulator.Execute(command, null);
                }
            }

            // rebuild robot from simulator robot and tracked state
            this.Robot = this.Simulator.TheRobot is null
                            ? null
                            : new ToyRobotModel(this.Simulator.TheRobot, robotState);

            return this.Robot;
        }
    }
}
