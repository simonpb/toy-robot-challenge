﻿using static ToyRobotChallenge.Library.Simulator;

namespace ToyRobotChallenge.Library.AppSupport.Application.Implementations.ExtraRobotCommands
{
    /// <summary>
    /// Not really a robot command, but used as a placeholder to capture the user's desire to "quit" the current simulation
    /// </summary>
    public class QuitCommand
        : IRobotCommand
    {
        public void Execute(IRobot robot, IStringResponse respond = null)
        {
            // anything to do?
        }
    }

    /// <summary>
    /// Not really a robot command, but used as a placeholder to capture the user's desire for help
    /// </summary>
    public class HelpCommand
        : IRobotCommand
    {
        public void Execute(IRobot robot, IStringResponse respond = null)
        {
            // anything to do?
        }
    }

    /// <summary>
    /// Special additional wave command
    /// </summary>
    public class WaveCommand
        : IRobotCommand
    {
        public bool? WaveToUser { get; }

        public WaveCommand(bool? waveToUser = null)
        {
            this.WaveToUser = waveToUser;
        }

        public void Execute(IRobot robot, IStringResponse respond = null)
        {
            // anything to do?
        }
    }

    /// <summary>
    /// Special additional jump command
    /// </summary>
    public class JumpCommand
        : IRobotCommand
    {
        public bool? DoJump { get; }

        public JumpCommand(bool? doJump = null)
        {
            this.DoJump = doJump;
        }

        public void Execute(IRobot robot, IStringResponse respond = null)
        {
            // anything to do?
        }
    }
}
