﻿using MediatR;
using System.Collections.Generic;
using ToyRobotChallenge.Library.AppSupport.Models;
using static ToyRobotChallenge.Library.Simulator;

/// <summary>
/// Standard "no frills" robot commands (requests)
/// </summary>
namespace ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests
{
    /// <summary>
    /// Start a simulation
    /// </summary>
    public class Start : IRequest<ToyRobotModel>
    {
        public ulong Rows { get; }
        public ulong Columns { get; }

        public Start(ulong rows, ulong columns)
        {
            this.Rows = rows;
            this.Columns = columns;
        }
    }

    /// <summary>
    /// Reset a simulation
    /// </summary>
    public class Reset : IRequest<ToyRobotModel>
    {
        public ulong Rows { get; }
        public ulong Columns { get; }

        public Reset(ulong rows, ulong columns)
        {
            this.Rows = rows;
            this.Columns = columns;
        }
    }

    /// <summary>
    /// Execute a single or collection of robot command(s)
    /// </summary>
    public class Execute : IRequest<ToyRobotModel>
    {
        public IEnumerable<IRobotCommand> Commands { get; }

        public Execute(IRobotCommand command)
        {
            this.Commands = new List<IRobotCommand>() { command };
        }

        public Execute(IEnumerable<IRobotCommand> commands)
        {
            this.Commands = commands;
        }
    }
}
