﻿using ToyRobotChallenge.Library.AppSupport.Models;
using MediatR;
using System.Threading;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Application.MediatR.Requests;
using ToyRobotChallenge.Library.AppSupport.Application.Interfaces;

/// <summary>
/// Standard "no frills" command (request) handlers
/// </summary>
namespace ToyRobotChallenge.Library.AppSupport.Application.MediatR.Handlers
{
    public class StartHandler
        : IRequestHandler<Start, ToyRobotModel>
    {
        private IToyRobotSimulator simulator { get; }

        public StartHandler(IToyRobotSimulator simulator)
        {
            this.simulator = simulator;
        }

        public async Task<ToyRobotModel> Handle(Start request, CancellationToken cancellationToken)
        {
            if (this.simulator.HasStarted == false)
            {
                await this.simulator.StartAsync(request.Rows, request.Columns);
            }

            return this.simulator?.Robot;
        }
    }

    public class ResetHandler
        : IRequestHandler<Reset, ToyRobotModel>
    {
        private IToyRobotSimulator simulator { get; }

        public ResetHandler(IToyRobotSimulator simulator)
        {
            this.simulator = simulator;
        }

        public async Task<ToyRobotModel> Handle(Reset request, CancellationToken cancellationToken)
        {
            if (this.simulator.HasStarted == true)
            {
                await this.simulator.ResetAsync(request.Rows, request.Columns);
            }

            return this.simulator?.Robot;
        }
    }

    public class ExecuteHandler
        : IRequestHandler<Execute, ToyRobotModel>
    {
        private IToyRobotSimulator simulator { get; }

        public ExecuteHandler(IToyRobotSimulator simulator)
        {
            this.simulator = simulator;
        }

        public async Task<ToyRobotModel> Handle(Execute request, CancellationToken cancellationToken)
        {
            if (this.simulator.HasStarted == true)
            {
                return await this.simulator.ExecuteAsync(request.Commands);
            }
            else
            {
                return null;
            }
        }
    }
}
