﻿using System.Collections.Generic;
using System.Threading.Tasks;
using ToyRobotChallenge.Library.AppSupport.Models;
using static ToyRobotChallenge.Library.Simulator;

namespace ToyRobotChallenge.Library.AppSupport.Application
{
    namespace Interfaces
    {
        public interface IToyRobotSimulator
        {
            ToyRobotModel Robot { get; }
            bool HasStarted { get; }
            Task StartAsync(ulong rows, ulong columns);
            Task ResetAsync(ulong rows, ulong columns);
            Task<ToyRobotModel> ExecuteAsync(IEnumerable<IRobotCommand> commands);
        }
    }
}
