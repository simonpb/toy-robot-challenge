﻿using ToyRobotChallenge.Library.Geometry;

namespace ToyRobotChallenge.Library.AppSupport.Models
{
    public class ToyRobotModel
    {
        public enum RobotState
        {
            None,
            Standing,
            Waving,
            Jumping,
        }

        public Vector2d<ulong> Vector { get; set; }
        public RobotState State { get; set; }

        public ToyRobotModel()
        {
            this.Vector = null;
            this.State = RobotState.None;
        }

        /// <summary>
        /// ctor from simulator <see cref="IRobot"/>
        /// </summary>
        /// <param name="robot"></param>
        /// <param name="state"></param>
        public ToyRobotModel(IRobot robot, RobotState state)
        {
            this.Vector = robot is null ? null : robot.Vector;
            this.State = robot is null ? RobotState.None : state;
        }
    }
}
