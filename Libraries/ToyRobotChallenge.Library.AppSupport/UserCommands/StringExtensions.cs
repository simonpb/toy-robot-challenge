﻿namespace ToyRobotChallenge.Library.AppSupport.UserCommands
{
    public static class StringExtensions
    {
        public static bool UserWantsToQuit(this string command)
        {
            // user wants help
            return (false
                    || string.Compare(command, "q", true) == 0
                    || string.Compare(command, "quit", true) == 0
                    || string.Compare(command, "exit", true) == 0);
        }

        public static bool UserWantsHelp(this string command)
        {
            // user quits
            return (false
                    || string.Compare(command, "h", true) == 0
                    || string.Compare(command, "help", true) == 0);
        }

        public static bool UserWantsReport(this string command)
        {
            // user want report
            return (string.Compare(command, "report", true) == 0);
        }

        public static bool UserWantsAWave(this string command)
        {
            // user wants help
            return (string.Compare(command, "wave", true) == 0);
        }

        public static bool UserWantsAJump(this string command)
        {
            // user wants help
            return (string.Compare(command, "jump", true) == 0);
        }
    }
}
