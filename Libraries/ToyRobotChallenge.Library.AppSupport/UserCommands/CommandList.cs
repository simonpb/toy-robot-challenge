﻿using ToyRobotChallenge.Library.AppSupport.Application.Implementations.ExtraRobotCommands;

namespace ToyRobotChallenge.Library.AppSupport.UserCommands
{
    public class CommandList
        : CommandLists.CommandList
    {
        public CommandList(string cmdString, bool ignoreCase) 
            // CAN process "unknown" commands
            : base(cmdString, ignoreCase, true)
        {
        }

        protected override Simulator.IRobotCommand ProcessUnknownCommand(string command)
        {
            if (string.IsNullOrWhiteSpace(command))
            {
                return null;
            }
            else if (command.UserWantsToQuit() == true)
            {
                return new QuitCommand();
            }
            else if (command.UserWantsAWave() == true)
            {
                return new WaveCommand();
            }
            else if (command.UserWantsAJump() == true)
            {
                return new JumpCommand();
            }
            else if (command.UserWantsHelp() == true)
            {
                return new HelpCommand();
            }
            else
            {
                return null;
            }
        }
    }
}
