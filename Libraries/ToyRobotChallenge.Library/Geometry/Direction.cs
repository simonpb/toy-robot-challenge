﻿namespace ToyRobotChallenge.Library.Geometry
{
    public enum Direction
    {
        North,
        South,
        East,
        West
    };
}
