﻿using ToyRobotChallenge.Library.Geometry;

namespace ToyRobotChallenge.Library
{
    /// <summary>
    /// A board for the robot to roam on
    /// </summary>
    public interface IBoard
    {
        ulong Height { get; }
        ulong Width { get; }

        bool IsValidPosition(Point2d<ulong> position);
    }
}