﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text.RegularExpressions;
using ToyRobotChallenge.Library.Geometry;
using static ToyRobotChallenge.Library.Simulator;

namespace ToyRobotChallenge.Library.CommandLists
{
    /// <summary>
    /// My command list, fill it by parsing a command string using a regular expression
    /// </summary>
    public class CommandList
        : List<IRobotCommand>
    {
        #region command strings
        private static string cmdStrPlace = "PLACE";
        private static string cmdStrValidate = "VALIDATE";
        private static string cmdStrMove = "MOVE";
        private static string cmdStrLeft = "LEFT";
        private static string cmdStrRight = "RIGHT";
        private static string cmdStrReport = "REPORT";

        private static string dirStrNorth = "NORTH";
        private static string dirStrSouth = "SOUTH";
        private static string dirStrEast = "EAST";
        // private static string dirStrWest = "WEST";
        #endregion

        // stuff for parsing the incoming command string
        private Regex _inputCmdRegEx;
        private int _regExGrpCommandIndex;
        private int _regExGrpXIndex;
        private int _regExGrpYIndex;
        private int _regExGrpFacingIndex;


        /// <summary>
        /// create a command list from a command string
        /// </summary>
        /// <param name="cmdString">the command string</param>
        /// <param name="ignoreCase">case sensitive?</param>
        /// <param name="canProcessUnkwnCmds">try to process "non stock" commands?</param>
        public CommandList(string cmdString, bool ignoreCase, bool canProcessUnkwnCmds = false)
        {
            // regular expression to parse the incoming command string in the format of
            // PLACE X,Y, FACING
            // VALIDATE X,Y, FACING
            // MOVE
            // LEFT
            // RIGHT
            // REPORT
            //
            // Note that the challange indicates that the PLACE command can optionally have spaces after the , between the Y value and the facing, so this expression
            // has been build with flexibility in mind.  There can be 0+ white spaces between the , after the Y and the facing value.
            // Commands case sensitive by default, but a quick tweek of the ctor can change all that!
            //
            // Also note that the VALIDATE command is addtional, so as to allow me to validate the current robot postion & facing agains that is expected
            // this means I can easily use external data set files to drive the validation of the code base

            // bit of an over kill, but lets use regular expressions to parse the incoming command strings
            var pattern = canProcessUnkwnCmds == false
                                // can ONLY process known stock commands
                                ? "((?<command>PLACE|VALIDATE)[\\s]* (?<x>[\\d]*)[\\s]*,[\\s]*(?<y>[\\d]*)[\\s]*,[\\s]*(?<facing>NORTH|SOUTH|EAST|WEST))|(?<command>MOVE|LEFT|RIGHT|REPORT)"
                                // CAN process unknown commands via overridden ProcessUnknownCommand method
                                : "((?<command>PLACE|VALIDATE)[\\s]* (?<x>[\\d]*)[\\s]*,[\\s]*(?<y>[\\d]*)[\\s]*,[\\s]*(?<facing>NORTH|SOUTH|EAST|WEST))|(?<command>MOVE|LEFT|RIGHT|REPORT|[\\S]+)";

            _inputCmdRegEx = new Regex(pattern, RegexOptions.Compiled | (ignoreCase ? RegexOptions.IgnoreCase : RegexOptions.None) | RegexOptions.Multiline);
            _regExGrpCommandIndex = _inputCmdRegEx.GroupNumberFromName("command");
            _regExGrpXIndex = _inputCmdRegEx.GroupNumberFromName("x");
            _regExGrpYIndex = _inputCmdRegEx.GroupNumberFromName("y");
            _regExGrpFacingIndex = _inputCmdRegEx.GroupNumberFromName("facing");

            BuildCommandList(cmdString, ignoreCase, canProcessUnkwnCmds);
        }

        /// <summary>
        /// Parse and build the command list from the command string
        /// </summary>
        /// <param name="cmdString">the command string</param>
        /// <param name="ignoreCase">ignore case?</param>
        private void BuildCommandList(string cmdString, bool ignoreCase, bool canProcessUnkwnCmds)
        {
            Debug.Assert(_inputCmdRegEx != null);

            // execute regex on command string
            var matches = _inputCmdRegEx.Matches(cmdString);

            foreach (var match in matches)
            {
                var groups = (match as Match).Groups;

                if (false
                    || string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrPlace, ignoreCase) == 0
                    || string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrValidate, ignoreCase) == 0)
                {
                    try
                    {
                        var x = ulong.Parse(groups[_regExGrpXIndex].Value);
                        var y = ulong.Parse(groups[_regExGrpYIndex].Value);
                        var dir = ToDirection(groups[_regExGrpFacingIndex].Value);

                        if (string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrPlace, ignoreCase) == 0)
                        {
                            // place the robot using a place command object
                            Add(new Simulator.PlaceCommand(new Vector2d<ulong>(new Point2d<ulong>(x, y), dir)));
                        }
                        else
                        {
                            // execute a location validation command
                            Add(new Simulator.ValidatePosAndDirCommand(new Vector2d<ulong>(new Point2d<ulong>(x, y), dir)));
                        }
                    }
                    catch (Exception e)
                        when (e is OverflowException || e is FormatException)
                    {
                        // catch potential overflow when parsing ulong and do nothing!
                    }
                }
                else if (string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrMove, ignoreCase) == 0)
                {
                    // execute a move command
                    Add(new MoveCommand());
                }
                else if (string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrLeft, ignoreCase) == 0)
                {
                    // execute a turn command
                    Add(new LeftTurnCommand());
                }
                else if (string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrRight, ignoreCase) == 0)
                {
                    // execute a turn command
                    Add(new RightTurnCommand());
                }
                else if (string.Compare(groups[_regExGrpCommandIndex].Value, cmdStrReport, ignoreCase) == 0)
                {
                    // execute a report command
                    Add(new ReportCommand());
                }
                else if(canProcessUnkwnCmds == true)
                {
                    var unknownCommand = ProcessUnknownCommand(groups[_regExGrpCommandIndex].Value);

                    if (unknownCommand != null)
                    {
                        // execute a "unknown" command
                        Add(unknownCommand);
                    }
                }
                else
                {
                    // should get to here, if the command didn't parse then, there shouldn't be any matches!
                    Debug.Assert(false);
                }
            }

            // local to convert "facing" string to direction enum
            Direction ToDirection(string facing)
            {
                if(string.Compare(facing, dirStrNorth, ignoreCase) == 0)
                {
                    return Direction.North;
                }
                else if(string.Compare(facing, dirStrEast, ignoreCase) == 0)
                {
                    return Direction.East;
                }
                else if(string.Compare(facing, dirStrSouth, ignoreCase) == 0)
                {
                    return Direction.South;
                }
                else
                {
                    return Direction.West;
                }
            }
        }

        /// <summary>
        /// override in derived class to handle "unknown" commands
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        virtual protected IRobotCommand ProcessUnknownCommand(string command)
        {
            return null;
        }
    }
}
