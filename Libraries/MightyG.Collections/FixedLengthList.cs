﻿using System.Collections;
using System.Collections.Generic;

namespace MightyG.Collections
{
    /// <summary>
    /// A simple class to represent a fixed length linked list
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class FixedLengthList<T>
        : IEnumerable<T>
    {
        private readonly int maxLength;
        private readonly List<T> buffer = new List<T>();

        /// <summary>
        /// Return number of items in the list
        /// </summary>
        public int Count => this.buffer.Count;

        /// <summary>
        /// Return item at specific index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public T this[int index] => this.buffer[index];

        /// <summary>
        /// ctor
        /// </summary>
        /// <param name="maxLength"></param>
        public FixedLengthList(int maxLength)
        {
            this.maxLength = maxLength;
        }

        /// <summary>
        /// ctor with seed items
        /// </summary>
        /// <param name="maxLength"></param>
        /// <param name="seedItems"></param>
        public FixedLengthList(int maxLength, IEnumerable<T> seedItems)
            : this(maxLength)
        {
            this.Add(seedItems);
        }

        /// <summary>
        /// Add a new item to the tail the list.  Excess items at the head of the list will be culled
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public int Add(T item)
        {
            // add single
            this.buffer.Add(item);

            CullExcessFromHead();

            return this.buffer.Count;
        }

        /// <summary>
        /// Add a new items to the tail the list.  Excess items at the head of the list will be culled
        /// </summary>
        /// <param name="items"></param>
        /// <returns></returns>
        public int Add(IEnumerable<T> items)
        {
            // add all
            this.buffer.AddRange(items);

            CullExcessFromHead();

            return this.buffer.Count;
        }

        private void CullExcessFromHead()
        {
            // move excess from the beginning
            if (this.buffer.Count > this.maxLength)
            {
                this.buffer.RemoveRange(0, this.buffer.Count - this.maxLength);
            }
        }

        #region IEnumerable<T>

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return this.buffer.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.buffer.GetEnumerator();
        }

        #endregion
    }
}
